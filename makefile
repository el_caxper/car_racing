# --------------------------------------------------------------------
# Makefile Genérico :: Módulo 2. Curso Experto Desarrollo Videojuegos
# Carlos González Morcillo     Escuela Superior de Informática (UCLM)
# --------------------------------------------------------------------
EXEC := car-racing

DIRSRC := src/
DIROBJ := obj/
DIRHEA := include/

CXX := g++

# Flags de compilación -----------------------------------------------
CXXFLAGS := -I$(DIRHEA) -std=c++11 -Wall -I/usr/local/include/cegui-0/CEGUI -I/usr/local/include/cegui-0 -I/usr/local/include/yaml-cpp -I/usr/include/OGRE/Overlay -I/usr/local/include/libcwiimote-0.4.0/libcwiimote `pkg-config --cflags OIS OGRE OgreBullet bullet OGRE-Overlay` -D_ENABLE_TILT -D_ENABLE_FORCE

# Flags del linker ---------------------------------------------------
LDFLAGS := `pkg-config --libs-only-L OGRE OgreBullet bullet OGRE-Overlay` -L/usr/local/lib
LDLIBS := `pkg-config --libs-only-l gl OIS OGRE SDL2_mixer OgreBullet bullet OGRE-Overlay` -lstdc++ -lIceUtil -lboost_system -lCEGUIBase-0 -lCEGUIOgreRenderer-0 -lyaml-cpp -lcwiimote -lbluetooth -lm -lIce -lIceStorm -pthread

# Modo de compilación (-mode=release -mode=debug) --------------------
ifeq ($(mode), release) 
	CXXFLAGS += -O2 -D_RELEASE
else 
	CXXFLAGS += -g -D_DEBUG
	mode := debug
endif

# Obtención automática de la lista de objetos a compilar -------------
OBJS := $(subst $(DIRSRC), $(DIROBJ), \
	$(patsubst %.cpp, %.o, $(wildcard $(DIRSRC)*.cpp)))

.PHONY: all clean

all: info dirs $(EXEC)

info:
	@echo '------------------------------------------------------'
	@echo '>>> Using mode $(mode)'
	@echo '    (Please, call "make" with [mode=debug|release])  '
	@echo '------------------------------------------------------'

dirs:
	mkdir -p $(DIROBJ)

# Enlazado -----------------------------------------------------------
$(EXEC): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# Compilación --------------------------------------------------------
$(DIROBJ)%.o: $(DIRSRC)%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Limpieza de temporales ---------------------------------------------
clean:
	rm -f *.log $(EXEC) *~ $(DIRSRC)*~ $(DIRHEA)*~ 
	rm -rf $(DIROBJ)


start: /usr/bin/icebox /usr/bin/icestormadmin
	@if ! [ -d db ]; then mkdir db; fi
	icebox --Ice.Config=icebox.cfg &
	sleep 3
	icestormadmin --Ice.Config=icestorm.cfg -e "create StateTopic"

stop:
	icestormadmin --Ice.Config=icestorm.cfg -e "destroy StateTopic"
	killall icebox

run:
	./$(EXEC)

