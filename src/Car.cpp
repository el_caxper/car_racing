#include <wiimote.h>
#include "Car.h"
#include "GameManager.h"

int Car::count = 0;

Car::Car(){}


Car::~Car(){
    printf("Destruyendo coche: %d\n", id);
    Car::count--;

}

Car::Car(Ogre::SceneManager* sceneMgr,OgreBulletDynamics::DynamicsWorld *world, Ogre::Vector3 pos, int* fcount){
    Car::count ++;
    printf("Creando coche: %d\n", Car::count);
    id = Car::count;
    frameCount = fcount;
    num_disp=0;
    targetPoint=0;


    const Ogre::Vector3 chassisShift(0, 1.5, 0);
    float connectionHeight = 0.7f;
    up = false;
    down = false;
    left = false;
    right = false;
    shoot = false;
    mSteering = 0.0f;
    _sceneMgr = sceneMgr;
    _world = world;
    _position = pos;

    mChassis = _sceneMgr->createEntity("chassis" + std::to_string(id), "chassis_lambo.mesh");
    if(id%4==1)     mChassis->getSubEntity(0)->setMaterialName("CarPaint_Physical_yellow");
    if(id%4==2)     mChassis->getSubEntity(0)->setMaterialName("CarPaint_Physical_purple");
    if(id%4==3)     mChassis->getSubEntity(0)->setMaterialName("CarPaint_Physical_blue");

    mChassis->setQueryFlags(RAY_QUERY_CAR);

    Ogre::SceneNode *node = _sceneMgr->createSceneNode("chassis" + std::to_string(id));
    _sceneMgr->getRootSceneNode()->addChild(node);

    chassisnode = _sceneMgr->createSceneNode("nodeChassisChild" + std::to_string(id));
    node->addChild(chassisnode);
    chassisnode->attachObject (mChassis);
    chassisnode->setPosition (chassisShift);
    mChassis->setVisibilityFlags(RAY_QUERY_CAR);

    Ogre::AxisAlignedBox boundingB = mChassis->getBoundingBox();
    Ogre::Vector3 size = boundingB.getSize();
    size /= 2.0f;

    chassisShape = new OgreBulletCollisions::BoxCollisionShape(size);
    compound = new OgreBulletCollisions::CompoundCollisionShape();
    compound -> addChildShape(chassisShape, chassisShift);

    mCarChassis = new OgreBulletDynamics::WheeledRigidBody("carChassis" + std::to_string(id), _world);

    camera = _sceneMgr->createCamera("Camera"+ std::to_string(id));
    camera->setPosition(_position + Ogre::Vector3(0,5,-15));
    camera->lookAt(_position);
    camera->setNearClipDistance(0.05);

    cameraNode = _sceneMgr->createSceneNode("cameraNode" + std::to_string(id));
    cameraNode->setPosition(camera->getPosition());
    chassisnode->addChild(cameraNode);
    Ogre::Vector3 aux;
    aux.x = _position.x * (id-1);
    aux.y = _position.y;
    aux.z = _position.z;
    cameraNode->setPosition(cameraNode->getPosition() - aux);
    mCarChassis->setShape (node, compound, 0.6, 0.6, 800, _position, Ogre::Quaternion::IDENTITY);
    mCarChassis->setDamping(0.2, 0.2);

    mTuning = new OgreBulletDynamics::VehicleTuning(20.f, 4.4f, 2.3f, 500.0, 10.5);
//    mTuning = new OgreBulletDynamics::VehicleTuning(20.f, 4.4f, 2.3f, 500.0, 100000.f, 10.5);

    mVehicleRayCaster = new OgreBulletDynamics::VehicleRayCaster(_world);
    mVehicle = new OgreBulletDynamics::RaycastVehicle(mCarChassis, mTuning, mVehicleRayCaster);

    mVehicle->setCoordinateSystem(0, 1, 2);

    Ogre::Vector3 wheelDirectionCS0(0,-1,0);
    Ogre::Vector3 wheelAxleCS(-1,0,0);


    for (size_t i = 0; i < 4; i++) {
        if(i==0) mWheels[i] = _sceneMgr->createEntity("wheel"  + std::to_string(i) + "_" + std::to_string(id), "FL_Wheel.mesh");
        if(i==1) mWheels[i] = _sceneMgr->createEntity("wheel"  + std::to_string(i) + "_" + std::to_string(id), "FR_Wheel.mesh");
        if(i==2) mWheels[i] = _sceneMgr->createEntity("wheel"  + std::to_string(i) + "_" + std::to_string(id), "RL_Wheel.mesh");
        if(i==3) mWheels[i] = _sceneMgr->createEntity("wheel"  + std::to_string(i) + "_" + std::to_string(id), "RR_Wheel.mesh");

        mWheels[i]->setCastShadows(true);
        mWheels[i]->setQueryFlags(RAY_QUERY_NONE);

        mWheelNodes[i] = _sceneMgr->createSceneNode("wheel"  + std::to_string(i) + "_" + std::to_string(id));
        _sceneMgr->getRootSceneNode()->addChild(mWheelNodes[i]);
        mWheelNodes[i]->attachObject (mWheels[i]);
    }

    bool isFrontWheel = true;

    /*Ogre::Vector3 connectionPointCS0 (0,0,0);*/
    Ogre::Vector3 connectionPointCS0 (1.56-(0.5*gWheelWidth),
                                      connectionHeight, 2.9-gWheelRadius);

    mVehicle->addWheel(mWheelNodes[0], connectionPointCS0, wheelDirectionCS0,
                       wheelAxleCS, gSuspensionRestLength, gWheelRadius,
                       isFrontWheel, gWheelFriction, gRollInfluence);

    connectionPointCS0 = Ogre::Vector3(-1.56+(0.5*gWheelWidth),
                                       connectionHeight, 2.9-gWheelRadius);

    mVehicle->addWheel(mWheelNodes[1], connectionPointCS0,
                       wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength,
                       gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);


    isFrontWheel = false;
    connectionPointCS0 = Ogre::Vector3(-1.56+(0.5*gWheelWidth),
                                       connectionHeight,-2.9+gWheelRadius);

    mVehicle->addWheel(mWheelNodes[2], connectionPointCS0,
                       wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength,
                       gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);

    connectionPointCS0 = Ogre::Vector3(1.56-(0.5*gWheelWidth),
                                       connectionHeight,-2.9+gWheelRadius);

    mVehicle->addWheel(mWheelNodes[3], connectionPointCS0,
                       wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength,
                       gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);


    carLifes = 5;
    Checkpoint1=false;


    Ogre::Entity *entityBall = _sceneMgr->createEntity("ballCar" +
       Ogre::StringConverter::toString(num_disp)+std::to_string(id), "blue_projectile.mesh");

    entityBall->setVisibilityFlags(CAR_BALL_MM);
    Ogre::SceneNode *nodeBall = _sceneMgr->createSceneNode("ballCar" +
       Ogre::StringConverter::toString(num_disp)+std::to_string(id));

    if(id%4==0)     entityBall->setMaterialName("CarPaint_Physical");
    if(id%4==1)     entityBall->setMaterialName("CarPaint_Physical_yellow");
    if(id%4==2)     entityBall->setMaterialName("CarPaint_Physical_purple");
    if(id%4==3)     entityBall->setMaterialName("CarPaint_Physical_blue");
    nodeBall->attachObject(entityBall);

    nodeBall->scale(100,100,100);
    nodeBall->setPosition(nodeBall->getPosition()+ Ogre::Vector3(0,50,0));
    chassisnode->addChild(nodeBall);
}


void Car::move(Player* p) {
  int _control = p->getControl();
    if(_control == IA_CONTROL){
        this->moveIA(p->getPointsIA());
        this->getRaycastVehicle()->setSteeringValue(this->getSteering(), 0);
        this->getRaycastVehicle()->setSteeringValue(this->getSteering(), 1);
    }
  if(_control == WIIMOTE_CONTROL) {
      wiimote_t wii_data = p->getWiimote()->getWiimoteData();
      // Throttle and brake
      up = wii_data.keys.two;
      down = wii_data.keys.one;
      // Shoot and horn
      if (*frameCount == 20) {
          this->setShoot(wii_data.keys.a);
          if(wii_data.keys.b)
              this->horn();
          *frameCount = 0;
      }
      //
      // Direction
      float angle=wii_data.force.y;
      float smoothAngle=angle*0.5;
      this->setSteering(smoothAngle);
      this->getRaycastVehicle()->setSteeringValue(this->getSteering(),0);
      this->getRaycastVehicle()->setSteeringValue(this->getSteering(),1);
  }
  if (this->getUp()) {
    this->getRaycastVehicle()->applyEngineForce(this->getEngineForce() , 0);
    this->getRaycastVehicle()->applyEngineForce(this->getEngineForce() , 1);
  } else if (this->getDown()) {
    this->getRaycastVehicle()->applyEngineForce(-this->getEngineForce() , 0);
    this->getRaycastVehicle()->applyEngineForce(-this->getEngineForce() , 1);
  }
  if (_control == KEYBOARD_CONTROL) {
    if (this->getLeft()) {
      if (this->getSteering() < 0.15) {
        this->setSteering(this->getSteering() + 0.04);
      }
    } else if (this->getRight()) {
      if (this->getSteering() > -0.15) {
        this->setSteering(this->getSteering() - 0.04);
      }
    } else {
      if (this->getSteering() > 0.0) {
        this->setSteering(this->getSteering() - 0.04);
      } else if (this->getSteering() < 0.0){
        this->setSteering(this->getSteering() + 0.04);
      } if (this->getSteering() > -0.05 && this->getSteering() < 0.05){
        this->setSteering(0.0);
      }
    }
      this->getRaycastVehicle()->setSteeringValue(this->getSteering(), 0);
      this->getRaycastVehicle()->setSteeringValue(this->getSteering(), 1);
  }
}

OgreBulletDynamics::RaycastVehicle* Car::getRaycastVehicle(){
  return mVehicle;
}
float Car::getSteering(){
  return mSteering;
}
bool Car::getUp(){
  return up;
}
bool Car::getDown(){
  return down;
}
bool Car::getLeft(){
  return left;
}
bool Car::getRight(){
  return right;
}
float Car::getEngineForce(){
  return gEngineForce;
}

bool Car::getShoot(){
  return shoot;
}

void Car::setUp(bool value){
  up=value;
}
void Car::setDown(bool value){
  down=value;
}
void Car::setLeft(bool value){
  left=value;
}
void Car::setRight(bool value){
  right=value;
}
void Car::setSteering(float value) {
  mSteering=value;
}
void Car::setShoot(bool value) {
  if(value)
    GameManager::getSingletonPtr()->getEffect(3)->play();
  shoot=value;
}

Ogre::Vector3 Car::getPosition(){
  return chassisnode->_getDerivedPosition();
}

Ogre::Camera* Car::getCamera(){
  return camera;
}

Ogre::SceneNode* Car::getCameraNode() {
  return cameraNode;
}

int Car::getId() {
  return id;
}

void Car::moveTo(Ogre::Vector3 pos) {
  btTransform transform;
  this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getMotionState()->getWorldTransform(transform);
  transform.setIdentity();
  transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(pos));
  this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->setWorldTransform(transform);

}

void Car::rotateAndMoveTo(Ogre::Radian angle,Ogre::Vector3 pos) {
  btTransform transform;
  this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getMotionState()->getWorldTransform(transform);
//  transform.setIdentity();
//  transform.setRotation(transform.getRotation()* btQuaternion(btVector3(0,1,0), btRadians(angle.valueDegrees())));
  transform.setIdentity();
  transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(pos));

  transform.setRotation(
                        OgreBulletCollisions::OgreBtConverter::to(Ogre::Quaternion(angle,Ogre::Vector3(0,1,0))));
    std::cout << "Angle recived2: " << angle.valueRadians() << "   Angle transform: " << transform.getRotation().getAngle() << std::endl;
  this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->setWorldTransform(transform);

}

void Car::rotateAndMoveTo(Ogre::Quaternion angle,Ogre::Vector3 pos) {
  btTransform transform;
  this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getMotionState()->getWorldTransform(transform);

  transform.setIdentity();

  transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(pos));

  transform.setRotation(OgreBulletCollisions::OgreBtConverter::to(angle));

  this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->setWorldTransform(transform);

}

void Car::rotate(Ogre::Degree angle) {

  btTransform transform;
  btQuaternion rotation, rotation2, result;
  this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getMotionState()->getWorldTransform(transform);
  rotation = transform.getRotation();
  rotation2 = btQuaternion(btVector3(0,1,0), btRadians(angle.valueDegrees()));
//  std::cout << "Giro de: " << angle << std::endl;
  result = rotation * rotation2;
  transform.setRotation(result);
  this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->setWorldTransform(transform);

}

Ogre::SceneNode* Car::getChassisSceneNode() {
    return chassisnode;
}

void Car::moveOnline(StateInfo si) {

  if (si.up) {
    this->getRaycastVehicle()->applyEngineForce(this->getEngineForce() , 0);
    this->getRaycastVehicle()->applyEngineForce(this->getEngineForce() , 1);
  } else if (si.down) {
    this->getRaycastVehicle()->applyEngineForce(-this->getEngineForce() , 0);
    this->getRaycastVehicle()->applyEngineForce(-this->getEngineForce() , 1);
  }

  this->setSteering(si.steering);
  this->getRaycastVehicle()->setSteeringValue(this->getSteering(), 0);
  this->getRaycastVehicle()->setSteeringValue(this->getSteering(), 1);

  //if (*frameCount == 4){
    this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getMotionState()->getWorldTransform(transform);
    transform.setRotation(btQuaternion(si.rotX, si.rotY, si.rotZ, si.rotAngle));
    transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(Ogre::Vector3(si.posX, si.posY, si.posZ)));
    this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->setWorldTransform(transform);
    //this->moveTo(Ogre::Vector3(si.posX, si.posY, si.posZ));
    //std::cout << "En moveTo: " << si.posX << " <-> "  << si.posY << " <-> "  << si.posZ << std::endl;
    //this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->setLinearVelocity(btVector3(si.lvX, si.lvY, si.lvZ));
    //this->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->setAngularVelocity(btVector3(si.avX, si.avY, si.avZ));
  //}

}


void Car::fire(){

  if (shoot){
    // create a particle system named explosions using the explosionTemplate
    if(!_sceneMgr->hasParticleSystem("fire"+std::to_string(num_disp)+std::to_string(id))){
      node2 = _sceneMgr->createSceneNode("fire"+std::to_string(num_disp)+std::to_string(id));

    }

    particleSystem = _sceneMgr->createParticleSystem("fire"+std::to_string(num_disp)+std::to_string(id), "Examples/JetEngine2");

    Ogre::Vector3 direc = Ogre::Vector3(this->getPosition().x-cameraNode->_getDerivedPosition().x,
                                        0,
                                        this->getPosition().z-cameraNode->_getDerivedPosition().z);
    particleSystem->getEmitter(0)->setPosition(this->getPosition()+direc.normalisedCopy()*10);
    particleSystem->getEmitter(0)->setDirection(direc);

    particleSystem->setDefaultDimensions(1,1);
    //particleSystem->setSpeedFactor(10);
    // fast forward 1 second  to the point where the particle has been emitted
    particleSystem->fastForward(1.0);

    // attach the particle system to a scene node
    node2->attachObject(particleSystem);
    _sceneMgr->getRootSceneNode()->addChild(node2);

    num_disp++;

    AddDynamicObject();

    shoot = false;
  }

}



void Car::AddDynamicObject() {

  Ogre::Vector3 size = Ogre::Vector3::ZERO;	// size of the box
  // starting position of the box

  Ogre::Vector3 direc = Ogre::Vector3(this->getPosition().x-cameraNode->_getDerivedPosition().x,
                                      0,
                                      this->getPosition().z-cameraNode->_getDerivedPosition().z);

  Ogre::Vector3 position = (this->getPosition()+
                            Ogre::Vector3(0,0,0)+
                            direc.normalisedCopy()*7);

  Ogre::Entity *entity = _sceneMgr->createEntity("Ball" +
     Ogre::StringConverter::toString(num_disp)+Ogre::StringConverter::toString(id), "blue_projectile.mesh");

  Ogre::SceneNode *node = _sceneMgr->createSceneNode("Ball" +
                                                     Ogre::StringConverter::toString(num_disp)+Ogre::StringConverter::toString(id));
  node->attachObject(entity);
  _sceneMgr->getRootSceneNode()->addChild(node);

  // Obtenemos la bounding box de la entidad creada... ------------
  Ogre::AxisAlignedBox boundingB = entity->getBoundingBox();
  size = boundingB.getSize();
//  size /= 2.0f;   // El tamano en Bullet se indica desde el centro

  size /= 4.0f;   // El tamano en Bullet se indica desde el centro

  // after that create the Bullet shape with the calculated size
  OgreBulletCollisions::BoxCollisionShape *boxShape = new
    OgreBulletCollisions::BoxCollisionShape(size);
  // and the Bullet rigid body
  OgreBulletDynamics::RigidBody *rigidBox = new
    OgreBulletDynamics::RigidBody("Ball" +
       Ogre::StringConverter::toString(num_disp)+std::to_string(id), _world);

//  rigidBox->getBulletRigidBody()->setGravity(btVector3(0,0,0));

  rigidBox->setShape(node, boxShape,
             0.6 /* Restitucion */, 0.1 /* Friccion */,
             1 /* Masa */, position /* Posicion inicial */,
             Ogre::Quaternion(0,0,0,1) /* Orientacion */);

  rigidBox->setLinearVelocity(direc * 7.0);

//  rigidBox->enableActiveState();
//      rigidBox->setKinematicObject(true);
      rigidBox->disableDeactivation();
//  rigidBox->applyImpulse(direc,rigidBox->getCenterOfMassPosition());

}

void Car::hide() {

    for (size_t i = 0; i < 4; i++) {
        mWheelNodes[i]->setVisible(false);
    }

    chassisnode->setVisible(false);

    die();

}


void Car::die(){


    GameManager::getSingletonPtr()->getEffect(4)->play();

    // create a particle system named explosions using the explosionTemplate
    if(!_sceneMgr->hasParticleSystem("explosions"+id)){
        node2Die = _sceneMgr->createSceneNode("explosion"+id);

    }

    particleSystemDie = _sceneMgr->createParticleSystem("explosions"+id, "explosionTemplate");

    particleSystemDie->setDefaultDimensions(5,5);
    particleSystemDie->setSpeedFactor(5);
    // fast forward 1 second  to the point where the particle has been emitted
    //particleSystem->fastForward(1.0);

    // attach the particle system to a scene node
    node2Die->attachObject(particleSystemDie);
    _sceneMgr->getRootSceneNode()->addChild(node2Die);
    node2Die->setPosition(this->getPosition());

    _die = true;

}

void Car::setState(enum Car::carState active){

    if(!mCarChassis->getBulletRigidBody()->isActive() && active==ACTIVE){
        std::cout << "Activando coche " << id << std::endl;
        mCarChassis->getBulletRigidBody()->forceActivationState(ACTIVE_TAG);
        mCarChassis->getBulletRigidBody()->forceActivationState(DISABLE_DEACTIVATION);
    }

    if(active==DESACTIVE){
        mCarChassis->getBulletRigidBody()->forceActivationState(WANTS_DEACTIVATION);
    }
}

void Car::horn() {
    GameManager::getSingletonPtr()->getEffect(5)->play();
}

float Car::getSpeedKMHour(){
    return (float) mVehicle->getBulletVehicle()->getCurrentSpeedKmHour();
}

void Car::moveIA(std::vector<PointIA>* ia_points) {
    // Decide whether we are closer to the current point or the next one
    Ogre::Vector3 currentPos=getPosition();
    bool cont=true;
    do {
        Ogre::Vector3 targetPos=Ogre::Vector3(ia_points->at(targetPoint).x,ia_points->at(targetPoint).y,ia_points->at(targetPoint).z);
        int nextPoint = (targetPoint + 1) % ia_points->size();
        Ogre::Vector3 nextPos = Ogre::Vector3(ia_points->at(nextPoint).x, ia_points->at(nextPoint).y,ia_points->at(nextPoint).z);
        if (currentPos.distance(nextPos) < currentPos.distance(targetPos)) {
            targetPoint = nextPoint;
        }
        else{
            cont=false;
        }
    } while(cont);
    // Apply the rules of the selected point
    this->up=ia_points->at(targetPoint).up;
    this->down=ia_points->at(targetPoint).down;
    this->mSteering=ia_points->at(targetPoint).steer;

    //std::cout << ia_points->at(targetPoint).x << " " << ia_points->at(targetPoint).z << " " << ia_points->at(targetPoint).up << " " << ia_points->at(targetPoint).steer << std::endl;

}
