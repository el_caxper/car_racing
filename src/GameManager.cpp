#include <Ogre.h>

#include "GameManager.h"
#include "GameState.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

template<> GameManager* Ogre::Singleton<GameManager>::msSingleton = 0;

GameManager::GameManager ()
{
  _root = 0;
  initSDL();
}

GameManager::~GameManager ()
{
  while (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }
  
  if (_root)
    delete _root;
}

void
GameManager::start
(GameState* state)
{
  // Creación del objeto Ogre::Root.
  _root = new Ogre::Root();
  
  loadResources();

  if (!configure())
    return;    
  	
  _inputMgr = new InputManager;
  _inputMgr->initialise(_renderWindow);

  // Inicialización del TrackManager y el SoundFXManager
  _pTrackManager = OGRE_NEW TrackManager;
  _pSoundFXManager = OGRE_NEW SoundFXManager;

  // Carga de sonidos
  _tracks=new std::vector<TrackPtr>();
  _tracks->push_back(_pTrackManager->load("sound/menu.wav")); // [0]
  _tracks->push_back(_pTrackManager->load("sound/gameplay.wav")); // [1]

  _effects=new std::vector<SoundFXPtr>();
  _effects->push_back(_pSoundFXManager->load("sound/button_click.wav")); // [0]
  _effects->push_back(_pSoundFXManager->load("sound/button_surface.wav")); // [1]
  _effects->push_back(_pSoundFXManager->load("sound/start_race_sem.wav")); // [2]
  _effects->push_back(_pSoundFXManager->load("sound/car_shoot.wav")); // [3]
  _effects->push_back(_pSoundFXManager->load("sound/car_die.wav")); // [4]
  _effects->push_back(_pSoundFXManager->load("sound/car_horn.wav")); // [5]
  _effects->push_back(_pSoundFXManager->load("sound/winning.wav")); // [6]
  _effects->push_back(_pSoundFXManager->load("sound/car_collision.wav")); // [7]

  // Registro como key y mouse listener...
  _inputMgr->addKeyListener(this, "GameManager");
  _inputMgr->addMouseListener(this, "GameManager");
  _inputMgr->addJoystickListener(this, "GameManager");

  // El GameManager es un FrameListener.
  _root->addFrameListener(this);

  // Transición al estado inicial.
  changeState(state);

  // Bucle de rendering.
  _root->startRendering();
}

void
GameManager::changeState
(GameState* state)
{
  // Limpieza del estado actual.
  if (!_states.empty()) {
    // exit() sobre el último estado.
    _states.top()->exit();
    // Elimina el último estado.
    _states.pop();
  }

  // Transición al nuevo estado.
  _states.push(state);
  // enter() sobre el nuevo estado.
  _states.top()->enter();
}

void
GameManager::pushState
(GameState* state)
{
  // Pausa del estado actual.
  if (!_states.empty())
    _states.top()->pause();
  
  // Transición al nuevo estado.
  _states.push(state);
  // enter() sobre el nuevo estado.
  _states.top()->enter();
}

void
GameManager::popState ()
{
  // Limpieza del estado actual.
  if (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }
  
  // Vuelta al estado anterior.
  if (!_states.empty())
    _states.top()->resume();
}

void
GameManager::loadResources ()
{
  Ogre::ConfigFile cf;
  cf.load("resources.cfg");
  
  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;    datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation
            (datastr, typestr, sectionstr);	
    }
  }
}

TrackPtr
GameManager::getTrack(int n){
    return _tracks->at(n);
}

SoundFXPtr
GameManager::getEffect(int n){
    return _effects->at(n);
}

bool
GameManager::configure ()
{
  if (!_root->restoreConfig()) {
    if (!_root->showConfigDialog()) {
      return false;
    }
  }
  
  _renderWindow = _root->initialise(true, "Car Racing");
  
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
  
  return true;
}

bool GameManager::initSDL () {
  if (SDL_Init(SDL_INIT_AUDIO) < 0) {
    return false;
  }
  // Llamar a  SDL_Quit al terminar.
  atexit(SDL_Quit);

  // Inicializando SDL mixer...
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0) {
    return false;
  }

  // Llamar a Mix_CloseAudio al terminar.
  atexit(Mix_CloseAudio);

  return true;
}

GameManager*
GameManager::getSingletonPtr ()
{
  return msSingleton;
}

GameManager&
GameManager::getSingleton ()
{  
  assert(msSingleton);
  return *msSingleton;
}

// Las siguientes funciones miembro delegan
// el evento en el estado actual.
bool
GameManager::frameStarted
(const Ogre::FrameEvent& evt)
{
  _inputMgr->capture();
  return _states.top()->frameStarted(evt);
}

bool
GameManager::frameEnded
(const Ogre::FrameEvent& evt)
{
  return _states.top()->frameEnded(evt);
}

bool
GameManager::keyPressed 
(const OIS::KeyEvent &e)
{
  _states.top()->keyPressed(e);
  return true;
}

bool
GameManager::keyReleased
(const OIS::KeyEvent &e)
{
  _states.top()->keyReleased(e);
  return true;
}

bool
GameManager::mouseMoved 
(const OIS::MouseEvent &e)
{
  _states.top()->mouseMoved(e);
  return true;
}

bool
GameManager::mousePressed 
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  _states.top()->mousePressed(e, id);
  return true;
}

bool
GameManager::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  _states.top()->mouseReleased(e, id);
  return true;
}

bool GameManager::povMoved(const OIS::JoyStickEvent &e, int pov) {
  _states.top()->povMoved(e, pov);
  return true;
}

bool GameManager::axisMoved(const OIS::JoyStickEvent &e, int axis) {
  int joystick = 0;
  int numDevices = _inputMgr->getNumOfJoysticks();
  for(int i = 0; i < numDevices; i++){
    if (e.device == _inputMgr->getJoystick(i)){
      joystick = i;
      break;
    }
  }
  _states.top()->axisMoved(e, axis, joystick);
  return true;
}

bool GameManager::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {
  _states.top()->sliderMoved(e, sliderID);
  return true;
}

bool GameManager::buttonPressed(const OIS::JoyStickEvent &e, int button) {
  int joystick = 0;
  int numDevices = _inputMgr->getNumOfJoysticks();
  for(int i = 0; i < numDevices; i++){
    if (e.device == _inputMgr->getJoystick(i)){
      joystick = i;
      break;
    }
  }
  _states.top()->buttonPressed(e, button, joystick);
  return true;
}

bool GameManager::buttonReleased(const OIS::JoyStickEvent &e, int button) {
  int joystick = 0;
  int numDevices = _inputMgr->getNumOfJoysticks();
  for(int i = 0; i < numDevices; i++){
    if (e.device == _inputMgr->getJoystick(i)){
      joystick = i;
      break;
    }
  }
  _states.top()->buttonReleased(e, button, joystick);
  return true;
}
