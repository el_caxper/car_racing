#include <WiimoteThread.h>
#include <iostream>
#include <fstream>
#include <wiimote.h>

WiimoteThread::WiimoteThread
        (){}

WiimoteThread::WiimoteThread
(int playerId){
    _wiimote = WIIMOTE_INIT;
    connect();
    connected=true;
    _wiimote.mode.acc = 1; // enable accelerometer
    _wiimote.led.bits = 1 << (playerId-1);
    _delay=IceUtil::Time::seconds(0);
    srand((unsigned)time(NULL));
}

void WiimoteThread::connect() {
    int status;
    do{
        status=wiimote_discover(&_wiimote,1);
    }while(status==WIIMOTE_ERROR);
    wiimote_connect(&_wiimote,_wiimote.link.r_addr);
}

void WiimoteThread::enableRumble(bool enable)
{
    int e = 0;
    if(enable) e=1;
    IceUtil::Mutex::Lock lock(_mutex);
    _wiimote.rumble=e;
}

wiimote_t WiimoteThread::getWiimoteData(){
    //IceUtil::Mutex::Lock lock(_mutex);
    return _wiimote;
}

int WiimoteThread::getKeysBits(){
    IceUtil::Mutex::Lock lock(_mutex);
    return _wiimote.keys.bits;
}

int WiimoteThread::getKeysOne(){
    //IceUtil::Mutex::Lock lock(_mutex);
    return _wiimote.keys.one;
}

int WiimoteThread::getKeysTwo(){
    //IceUtil::Mutex::Lock lock(_mutex);
    return _wiimote.keys.two;
}

int WiimoteThread::getKeysPlus(){
    IceUtil::Mutex::Lock lock(_mutex);
    return _wiimote.keys.plus;
}

int WiimoteThread::getKeysMinus(){
    IceUtil::Mutex::Lock lock(_mutex);
    return _wiimote.keys.minus;
}

void WiimoteThread::setConnected(bool b){
    connected=b;
}

double WiimoteThread::getAngleZ()
{
  IceUtil::Mutex::Lock lock(_mutex);
  return _wiimote.tilt.z;
}

void
WiimoteThread::update ()
{
  IceUtil::Mutex::Lock lock(_mutex);
  if (wiimote_is_open(&_wiimote)) {
          wiimote_update(&_wiimote);
  }
  else{
      connected=false;
  }
}

void
WiimoteThread::run ()
{
    // Enable wiimote player 1 led
    while(connected) {
        // Update wiimote struct
        update();
        IceUtil::ThreadControl::sleep(_delay);
    }
    wiimote_disconnect(&_wiimote);
}
