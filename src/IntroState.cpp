#include "IntroState.h"
#include "MenuState.h"
#include <iostream>
#include <yaml-cpp/yaml.h>


template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

void
IntroState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");

    carga_recursosCEGUI();
    changeState(MenuState::getSingletonPtr());

    _exitGame = false;


}

void
IntroState::exit()

{
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void
IntroState::pause ()
{
}

void
IntroState::resume ()
{
}

bool
IntroState::frameStarted
(const Ogre::FrameEvent& evt) 
{
    return true;
}

bool
IntroState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

void
IntroState::keyPressed
(const OIS::KeyEvent &e)
{
 
}

void
IntroState::keyReleased
(const OIS::KeyEvent &e )
{
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void
IntroState::mouseMoved
(const OIS::MouseEvent &e)
{
}

void
IntroState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void
IntroState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
}

void IntroState::povMoved(const OIS::JoyStickEvent &e, int pov) {

}

void IntroState::axisMoved(const OIS::JoyStickEvent &e, int axis, int joystick) {

}

void IntroState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {

}

void IntroState::buttonPressed(const OIS::JoyStickEvent &e, int button, int joystick) {

}

void IntroState::buttonReleased(const OIS::JoyStickEvent &e, int button, int joystick) {

}

IntroState*
IntroState::getSingletonPtr ()
{
    return msSingleton;
}

IntroState&
IntroState::getSingleton ()
{ 
    assert(msSingleton);
    return *msSingleton;
}

void IntroState::carga_recursosCEGUI(){
    renderer = &CEGUI::OgreRenderer::bootstrapSystem();
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
    CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
    CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
    CEGUI::SchemeManager::getSingleton().createFromFile("Gustavo.scheme");
    CEGUI::SchemeManager::getSingleton().createFromFile("Generic.scheme");
    CEGUI::SchemeManager::getSingleton().createFromFile("GameMenu.scheme");
    CEGUI::SchemeManager::getSingleton().createFromFile("OgreTray.scheme");
    CEGUI::SchemeManager::getSingleton().createFromFile("WindowsLook.scheme");
    CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
    CEGUI::SchemeManager::getSingleton().createFromFile("SampleBrowser.scheme");
    CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(-mousePos.d_x,-mousePos.d_y);
}





