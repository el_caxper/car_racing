#include "OnlinePublisher.h"

OnlinePublisher::OnlinePublisher()
{
    try {
        int argc=2;
        char* argv[]={"OnlinePublisher","--Ice.Config=icestorm.cfg"};

        ic = Ice::initialize(argc, argv);

        string key = "IceStormAdmin.TopicManager.Default";
        ObjectPrx base = ic->propertyToProxy(key);
        /*
        if (!base) {
            cerr << "property " << key << " not set." << endl;
            return 0;
        }
         */

        cout << "Using IceStorm in: '" << key << "' " << endl;
        TopicManagerPrx topic_mgr = TopicManagerPrx::checkedCast(base);

        /*
        if (!topic_mgr) {
            cerr << "invalid proxy" << endl;
            return EXIT_FAILURE;
        }
         */

        TopicPrx topic;
        try {
            topic = topic_mgr->retrieve("StateTopic");
        } catch (const NoSuchTopic& e) {
            cerr << "no such topic found, creating" << endl;
            topic = topic_mgr->create("StateTopic");
        }

        assert(topic);

        ObjectPrx prx = topic->getPublisher();
        statePrx = StatePrx::uncheckedCast(prx);

        statePrx->setStart(true);
        std::cout << "Envianto TRUE\n" << std::endl;

    } catch (const Ice::Exception& ex) {
        cerr << ex << endl;
    } catch (const char* msg) {
        cerr << msg << endl;
    }

}

OnlinePublisher::~OnlinePublisher() {
    if (ic)
        ic->destroy();
}

StatePrx OnlinePublisher::getStatePrx() {
    return statePrx;
}
