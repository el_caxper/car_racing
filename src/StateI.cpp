#include "StateI.h"

StateI::StateI(std::vector<StateInfo>* vector, bool *start) {
    _playersInfo = vector;
    _start = start;
}

void StateI::sendState(const StateInfo& s, const Current& current) {
  _playersInfo->at(s.iden).up = s.up;
  _playersInfo->at(s.iden).down = s.down;
  _playersInfo->at(s.iden).steering = s.steering;
  _playersInfo->at(s.iden).posX = s.posX;
  _playersInfo->at(s.iden).posY = s.posY;
  _playersInfo->at(s.iden).posZ = s.posZ;
  _playersInfo->at(s.iden).rotX = s.rotX;
  _playersInfo->at(s.iden).rotY = s.rotY;
  _playersInfo->at(s.iden).rotZ = s.rotZ;
  _playersInfo->at(s.iden).rotAngle = s.rotAngle;
  _playersInfo->at(s.iden).shoot = s.shoot;
  /*_playersInfo->at(s.iden).avX = s.avX;
  _playersInfo->at(s.iden).avY = s.avY;
  _playersInfo->at(s.iden).avZ = s.avZ;
  _playersInfo->at(s.iden).lvX = s.lvX;
  _playersInfo->at(s.iden).lvY = s.lvY;
  _playersInfo->at(s.iden).lvZ = s.lvZ;*/

}

void StateI::setStart(const bool start , const Current& current){
    *_start = start;
    //std::cout << "StateI: Received: " <<start << " Saved: " << *_start<< std::endl;
}




