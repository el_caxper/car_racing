#include "Player.h"

int Player::count = 0;

Player::Player(){}

Player::~Player(){
    printf("Destruyendo player: %d\n", id);
    Player::count--;
    if(controlType == WIIMOTE_CONTROL) {
        printf("Desactivando mando del player: %d\n", id);
        wii->setConnected(false);
    }
}

Player::Player(int vehicle, int control){
    Player::count ++;
    printf("Creando player: %d\n", Player::count);
    id = Player::count;

    vehicleId = vehicle;
    controlType = control;

    // Create and launch Wiimote thread
    if(controlType == WIIMOTE_CONTROL) {
        wii = new WiimoteThread(id);
        IceUtil::ThreadControl tc = wii->start();
        tc.detach();
    }
    else if(controlType==IA_CONTROL){
        v_points=new std::vector<PointIA>();
        FILE *f;
        f=fopen("ia_route_v1.txt","r");
        float x,y,z;
        int up,down;
        double steer;
        while(((fscanf(f,"%f,%f,%f,%d,%d,%lf",&x,&y,&z,&up,&down,&steer))!=EOF)){
            PointIA pia=PointIA();
            pia.x=x;
            pia.y=y;
            pia.z=z;
            pia.up=up;
            pia.down=down;
            pia.steer=steer;
            v_points->push_back(pia);
            //printf("%d,%d,%d,%d,%d,%lf\n",x,y,z,up,down,steer);
        }
    }

}

std::vector<PointIA>* Player::getPointsIA() {
    return v_points;
}

int Player::getId() {
  return id;
}

int Player::getVehicleId() {
  return vehicleId;
}

int Player::getControl() {
  return controlType;
}

WiimoteThreadPtr Player::getWiimote() {
    return wii;
}




