#include "MenuState.h"
#include "PlayState.h"


template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = 0;

void
MenuState::enter ()
{

    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");

    //    carga_recursosCEGUI();
    createGUI();

    // Inicia track principal
    if(_music)
        GameManager::getSingletonPtr()->getTrack(1)->stop();
        GameManager::getSingletonPtr()->getTrack(0)->play();


    _exitGame = false;
}

void
MenuState::exit()
{
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();

    std::cout << "Num Animaciones PLAY" <<CEGUI::AnimationManager::getSingleton().getNumAnimations()<< std::endl;
        std::cout << "Num Inst Animaciones PLAY" <<CEGUI::AnimationManager::getSingleton().getNumAnimationInstances()<< std::endl;

        if(CEGUI::AnimationManager::getSingleton().getNumAnimations()<13 &&
                CEGUI::AnimationManager::getSingleton().getNumAnimationInstances()>4){
            CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella");
            CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella2");
            CEGUI::AnimationManager::getSingleton().destroyAnimation("ViajeMeteorito");
            CEGUI::AnimationManager::getSingleton().destroyAnimation("LoopRotateRight");
        }

    CEGUI::WindowManager::getSingleton().destroyAllWindows();
}

void
MenuState::pause ()
{
}

void
MenuState::resume ()
{
}

bool
MenuState::frameStarted
(const Ogre::FrameEvent& evt) 
{
    // Make sure the CEGUI System is initialized and running
    // and if it is, inject a time pulse
    if ( CEGUI::System::getSingletonPtr() )
        CEGUI::System::getSingleton().injectTimePulse( evt.timeSinceLastFrame);
    // From the 0.8 version (next to the line above)
    CEGUI::System::getSingleton().getDefaultGUIContext().injectTimePulse( evt.timeSinceLastFrame );
    return true;
}

bool
MenuState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

void
MenuState::keyPressed
(const OIS::KeyEvent &e)
{
    // Transición al siguiente estado.
    // Espacio --> PlayState
    if (e.key == OIS::KC_SPACE) {
        changeState(PlayState::getSingletonPtr());
    }

    if(e.key == OIS::KC_DOWN){
        std::cout << "Activando boton de juego" << std::endl;

        std::cout << "Posicion: " <<
                     configWin->getChild("Menu")->getChild("PlayButton")->getYPosition().d_scale << "  " <<
                     configWin->getChild("Menu")->getChild("PlayButton")->getYPosition().d_offset << std::endl;
//        configWin->getChild("Menu")->getChild("RecordButton")->activate();
        CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(
                    configWin->getChild("Menu")->getChild("PlayButton")->getXPosition().d_scale,
                    configWin->getChild("Menu")->getChild("PlayButton")->getYPosition().d_scale);
    }

    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown((CEGUI::Key::Scan)e.key);
}

void
MenuState::keyReleased
(const OIS::KeyEvent &e )
{
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp((CEGUI::Key::Scan)e.key);
}

void
MenuState::mouseMoved
(const OIS::MouseEvent &e)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void
MenuState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void
MenuState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

void MenuState::povMoved(const OIS::JoyStickEvent &e, int pov) {

}

void MenuState::axisMoved(const OIS::JoyStickEvent &e, int axis, int joystick) {

}

void MenuState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {

}

void MenuState::buttonPressed(const OIS::JoyStickEvent &e, int button, int joystick) {

}

void MenuState::buttonReleased(const OIS::JoyStickEvent &e, int button, int joystick) {

}

MenuState*
MenuState::getSingletonPtr ()
{
    return msSingleton;
}

MenuState&
MenuState::getSingleton ()
{ 
    assert(msSingleton);
    return *msSingleton;
}


CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
    case OIS::MB_Left:
        ceguiId = CEGUI::LeftButton;
        break;
    case OIS::MB_Right:
        ceguiId = CEGUI::RightButton;
        break;
    case OIS::MB_Middle:
        ceguiId = CEGUI::MiddleButton;
        break;
    default:
        ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

void MenuState::createGUI()
{
    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","menu_prin");
    configWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("main_menu.layout");

    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setVisible(true);

    CEGUI::Window* menu = configWin->getChild("Menu");
    CEGUI::Window* exitButton = menu->getChild("ExitButton");
    exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                               CEGUI::Event::Subscriber(&MenuState::quit,
                                                        this));
    exitButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                             CEGUI::Event::Subscriber(&MenuState::onMouseEnters,   this));

    CEGUI::Window* playButton = menu->getChild("PlayButton");
    playButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                               CEGUI::Event::Subscriber(&MenuState::play,
                                                        this));
    playButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                             CEGUI::Event::Subscriber(&MenuState::onMouseEnters,   this));


    CEGUI::Window* recordButton = menu->getChild("RecordButton");
    recordButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                                 CEGUI::Event::Subscriber(&MenuState::show_records,
                                                          this));
    recordButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                             CEGUI::Event::Subscriber(&MenuState::onMouseEnters,   this));

    CEGUI::Window* CreditButton = menu->getChild("CreditsButton");
    CreditButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                                 CEGUI::Event::Subscriber(&MenuState::show_credits,
                                                          this));
    CreditButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                             CEGUI::Event::Subscriber(&MenuState::onMouseEnters,   this));


    CEGUI::Window* ConfigButton = menu->getChild("ConfigButton");
    ConfigButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                                 CEGUI::Event::Subscriber(&MenuState::show_config,
                                                          this));
    ConfigButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                             CEGUI::Event::Subscriber(&MenuState::onMouseEnters,   this));

    CEGUI::Window* Imagen_estrella2 = configWin->getChild("Imagen_estrella2");
    CEGUI::Window* Imagen_estrella = configWin->getChild("Imagen_estrella");
    CEGUI::Window* Imagen_meteor = configWin->getChild("Imagen_meteor");
    CEGUI::Window* Golf_club = configWin->getChild("Golf_club");

    Golf_club->moveToBack();

    sheet->addChild(configWin);
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);

    std::cout << "Numero anumaciones = MENU CREATE" << CEGUI::AnimationManager::getSingleton().getNumAnimations() << std::endl;
    std::cout << "Numero instancias anumaciones = MENU CREATE" << CEGUI::AnimationManager::getSingleton().getNumAnimationInstances() << std::endl;

    if (CEGUI::AnimationManager::getSingleton().getNumAnimations()==8 ){
        CEGUI::AnimationManager::getSingleton().loadAnimationsFromXML("GameMenu.anims");
        MenuState::parpadeo_anim = CEGUI::AnimationManager::getSingleton().instantiateAnimation("ParpadeoEstrella");
        MenuState::parpadeo_rot_anim = CEGUI::AnimationManager::getSingleton().instantiateAnimation("ParpadeoEstrella2");
        MenuState::viaje_meteor = CEGUI::AnimationManager::getSingleton().instantiateAnimation("ViajeMeteorito");
        MenuState::rotacion_loop = CEGUI::AnimationManager::getSingleton().instantiateAnimation("LoopRotateRight");

        // after we instantiate the animation, we have to set its target window
    }

    MenuState::parpadeo_anim->setTargetWindow(Imagen_estrella);
    MenuState::parpadeo_rot_anim->setTargetWindow(Imagen_estrella2);
    MenuState::viaje_meteor->setTargetWindow(Imagen_meteor);
    MenuState::rotacion_loop->setTargetWindow(Golf_club);


    // at this point, you can start this instance and see the results
    MenuState::parpadeo_anim->start();
    MenuState::parpadeo_rot_anim->start();
    MenuState::viaje_meteor->start();

    _config_window_atcive = false;

}

bool MenuState::quit(const CEGUI::EventArgs &e)
{
    GameManager::getSingletonPtr()->getEffect(0)->play();
    _exitGame = true;
    return true;
}


bool MenuState::play(const CEGUI::EventArgs &e)
{
    GameManager::getSingletonPtr()->getEffect(0)->play();

    changeState(PlayState::getSingletonPtr());
    return true;
}

bool MenuState::show_records(const CEGUI::EventArgs &e)
{
    GameManager::getSingletonPtr()->getEffect(0)->play();
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    mostrar_records();
    return true;
}


bool MenuState::back_menu(const CEGUI::EventArgs &e)
{
    GameManager::getSingletonPtr()->getEffect(0)->play();
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    createGUI();
    return true;
}


void MenuState::mostrar_records()
{
    CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella");
    CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella2");
    CEGUI::AnimationManager::getSingleton().destroyAnimation("ViajeMeteorito");
    CEGUI::AnimationManager::getSingleton().destroyAnimation("LoopRotateRight");


    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","records");
    CEGUI::Window* configWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("ventane_records.layout");
    CEGUI::Window* exitButton = configWin->getChild("ExitButton");
    exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                               CEGUI::Event::Subscriber(&MenuState::back_menu,
                                                        this));

    exitButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                             CEGUI::Event::Subscriber(&MenuState::onMouseEnters,   this));


    using namespace CEGUI;
    MultiColumnList* multiColumnList = static_cast<MultiColumnList*>(configWin->getChild("MultiColumnList"));\
    multiColumnList->addColumn("Jugador", 0, UDim(0.6f, 0));
    multiColumnList->addColumn("Tiempo", 1, UDim(0.3f, 0));
//    multiColumnList->addColumn("Tiempo", 2, UDim(0.25f, 0));
    multiColumnList->setAutoSizeColumnUsesHeader(true);
    multiColumnList->setSelectionMode(MultiColumnList::RowSingle);
    multiColumnList->setVerticalAlignment(VA_CENTRE);
    multiColumnList->setHorizontalAlignment(HA_CENTRE);

    ListboxTextItem* itemMultiColumnList;
    multiColumnList->addRow();

    YAML::Node doc = YAML::LoadFile("./records.yaml");
    int col= 0;
    for (YAML::const_iterator it = doc.begin(); it != doc.end(); ++it) {
        YAML::Node key = it->first;
        multiColumnList->addRow();

        if (key.Type() == YAML::NodeType::Scalar) {

            itemMultiColumnList = new ListboxTextItem(key.as<std::string>(), 1);
            multiColumnList->setItem(itemMultiColumnList, 0, col); // ColumnID, RowID

            itemMultiColumnList = new ListboxTextItem(doc[key][0].as<std::string>()+"[horz-alignment='centre']", 2);
            multiColumnList->setItem(itemMultiColumnList, 1, col); // ColumnID, RowID

        }
        col++;
    }

    multiColumnList->setVerticalAlignment(VA_CENTRE);
    multiColumnList->setHorizontalAlignment(HA_CENTRE);

    sheet->addChild(configWin);
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);
}

void MenuState::show_credits(){

    GameManager::getSingletonPtr()->getEffect(0)->play();

    CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella");
    CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella2");
    CEGUI::AnimationManager::getSingleton().destroyAnimation("ViajeMeteorito");
    CEGUI::AnimationManager::getSingleton().destroyAnimation("LoopRotateRight");

    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","creditos");
    CEGUI::Window* configWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("ventana_creditos.layout");
    CEGUI::Window* exitButton = configWin->getChild("ExitButton");
    exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                               CEGUI::Event::Subscriber(&MenuState::back_menu,
                                                        this));

    exitButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                             CEGUI::Event::Subscriber(&MenuState::onMouseEnters,   this));

    using namespace CEGUI;
    CEGUI::Window* staticText = static_cast<Window*>(configWin->getChild("StaticText"));

    staticText->setText("Juego Car-Racing para el Curso de Experto en Desarrollo de Videojuegos 6 Edicion 2016/2017\n"
                        "Desarrolladores:\n   "
                        "[horz-alignment='left']-    [horz-alignment='left']Jesus [horz-alignment='left']Ruiz-Santaquiteria "
                        "[horz-alignment='left']\n   "
                        "[horz-alignment='left']-    [horz-alignment='left']Gustavo [horz-alignment='left']Plaza   "
                        "[horz-alignment='left']\n   "
                        "[horz-alignment='left']-    [horz-alignment='left']Anibal [horz-alignment='left']Pedraza   "
                        "[horz-alignment='left']\n   ");

    staticText->setProperty("TextColours", "tl:FFFF0000 tr:FFFF0000 bl:FFFF0000 br:FFFF0000");
    staticText->setProperty("VertFormatting", "VertCentred"); // TopAligned, BottomAligned, VertCentred
    staticText->setProperty("HorzFormatting", "WordWrapJustified"); // LeftAligned, RightAligned, HorzCentred
    // HorzJustified, WordWrapLeftAligned, WordWrapRightAligned, WordWrapCentred, WordWrapJustified

    sheet->addChild(configWin);
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);

}


bool MenuState::onMouseEnters(const CEGUI::EventArgs& e)
{
    /*CODIGO AL ENTRAR AL BOTON*/
    GameManager::getSingletonPtr()->getEffect(1)->play();
    return true;
}

void MenuState::show_config(){

    GameManager::getSingletonPtr()->getEffect(0)->play();

    using namespace CEGUI;
    CEGUI::Window* menu_config;


      // Read data from file
    std::ifstream conf("conf.txt");
    conf >> _mode;
    conf >> _control_1;
    conf >> _control_2;
    conf >> _music;
    conf >> _numCars;
    conf >> _id;

    if( _config_window_atcive){

        menu_config = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("Menu_config");

    }else{

        menu_config = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("main_menu.layout")->getChild("Menu_config");

        /* Combobox */
        combobox_tipo_juego = static_cast<Combobox*>(menu_config->getChild("DropDownMenu_jugadores"));
        combobox_tipo_juego->setReadOnly(true);
        ListboxTextItem* itemCombobox = new ListboxTextItem("UN JUGADOR", 1);
        itemCombobox->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
        combobox_tipo_juego->addItem(itemCombobox);
        if (_mode==1) combobox_tipo_juego->setText(itemCombobox->getText());
        itemCombobox = new ListboxTextItem("MULTIJUGADOR", 2);
        itemCombobox->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
        combobox_tipo_juego->addItem(itemCombobox);
        if (_mode==2) combobox_tipo_juego->setText(itemCombobox->getText());
        itemCombobox = new ListboxTextItem("ONLINE", 3);
        itemCombobox->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
        combobox_tipo_juego->addItem(itemCombobox);
        if (_mode==3) combobox_tipo_juego->setText(itemCombobox->getText());

        combobox_tipo_juego->subscribeEvent(CEGUI::ComboDropList::EventListSelectionAccepted,
                                     CEGUI::Event::Subscriber(&MenuState::game_mode_changed,
                                                              this));

        /* Combobox */
        combobox_control_1 = static_cast<Combobox*>(menu_config->getChild("DropDownMenu_mando"));
        combobox_control_1->setReadOnly(true);
        ListboxTextItem* itemCombobox_c = new ListboxTextItem("MANDO", 1);
        itemCombobox_c->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
        combobox_control_1->addItem(itemCombobox_c);
        if (_control_1==1) combobox_control_1->setText(itemCombobox_c->getText()); // Copy the item's text into the Editbox
        itemCombobox_c = new ListboxTextItem("TECLADO", 2);
        itemCombobox_c->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
        combobox_control_1->addItem(itemCombobox_c);
        if (_control_1==2) combobox_control_1->setText(itemCombobox_c->getText()); // Copy the item's text into the Editbox
        itemCombobox_c = new ListboxTextItem("WIIMOTE", 3);
        itemCombobox_c->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
        combobox_control_1->addItem(itemCombobox_c);
        if (_control_1==3) combobox_control_1->setText(itemCombobox_c->getText()); // Copy the item's text into the Editbox


         combobox_control_2 = static_cast<Combobox*>(menu_config->getChild("DropDownMenu_mando3"));
         combobox_control_2->setReadOnly(true);
         itemCombobox_c = new ListboxTextItem("MANDO", 1);
         itemCombobox_c->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
         combobox_control_2->addItem(itemCombobox_c);
         if (_control_2==1) combobox_control_2->setText(itemCombobox_c->getText()); // Copy the item's text into the Editbox
         itemCombobox_c = new ListboxTextItem("TECLADO", 2);
         itemCombobox_c->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
         combobox_control_2->addItem(itemCombobox_c);
         if (_control_2==2) combobox_control_2->setText(itemCombobox_c->getText()); // Copy the item's text into the Editbox
         itemCombobox_c = new ListboxTextItem("WIIMOTE", 3);
         itemCombobox_c->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
         combobox_control_2->addItem(itemCombobox_c);
         if (_control_2==3) combobox_control_2->setText(itemCombobox_c->getText()); // Copy the item's text into the Editbox


         if(_mode==2) game_mode_changed();

        check_music = static_cast<ToggleButton*>(menu_config->getChild("CheckboxMusic"));

        if(_music) check_music->setSelected( true );

        CEGUI::Window* applyButton = menu_config->getChild("AplicarButton");
        applyButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                                   CEGUI::Event::Subscriber(&MenuState::aplicar_conf,
                                                            this));
        applyButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                                 CEGUI::Event::Subscriber(&MenuState::onMouseEnters,   this));

        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(menu_config);

        _config_window_atcive = true;
    }


    if(menu_config->isVisible()){
        menu_config->hide();
        std::cout << "OCULTANDO MENU CONF" << std::endl;
    }else {
         menu_config->show();
         menu_config->moveToFront();
         std::cout << "MOSTRANDO MENU CONF" << std::endl;
    }

}


void MenuState::aplicar_conf(){

        std::ofstream fout;
        fout.open("conf.txt");
        if(combobox_tipo_juego->isItemSelected(0)|| combobox_tipo_juego->isItemSelected(1)||combobox_tipo_juego->isItemSelected(2))
            fout<<combobox_tipo_juego->getSelectedItem()->getID()<<std::endl;
        else
            fout<<1<<std::endl;

        if(combobox_control_1->isItemSelected(0)|| combobox_control_1->isItemSelected(1) || combobox_control_1->isItemSelected(2))
            fout<<combobox_control_1->getSelectedItem()->getID()<<std::endl;
        else
          fout<<1<<std::endl;

        if((combobox_control_2->isItemSelected(0)|| combobox_control_2->isItemSelected(1) || combobox_control_2->isItemSelected(2))
                && combobox_tipo_juego->isItemSelected(1))
            fout<<combobox_control_2->getSelectedItem()->getID()<<std::endl;
        else
          fout<<9<<std::endl;

        if (check_music->isSelected()){
          fout<<1<<std::endl;
        }else {
          fout<<0<<std::endl;
        }
        fout<<_numCars<<std::endl;
        fout<< _id <<std::endl;
        fout.close();
}


void MenuState::game_mode_changed(){

        std::cout << "SELECCIONADO UN MODO DE JUEGO" << std::endl;

        if(combobox_tipo_juego->isItemSelected(1)){
            combobox_control_1->setPosition(CEGUI::UVector2(CEGUI::UDim(-0.22,0),CEGUI::UDim(0.3,0)));
            combobox_control_1->setSize(CEGUI::USize(CEGUI::UDim(0.45,0),CEGUI::UDim(0.5,0)));
            combobox_control_2->show();

        }else{
            if(combobox_control_2->isVisible()) combobox_control_2->hide();
            combobox_control_1->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0.3,0)));
            combobox_control_1->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.5,0)));
        }
}
