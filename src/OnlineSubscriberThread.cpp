#include "OnlineSubscriberThread.h"

OnlineSubscriberThread::OnlineSubscriberThread(int numPlayers) {

    _numPlayers = numPlayers;
    _start = false;

    for (int i = 0; i < _numPlayers; i++){
        StateInfo si = StateInfo();
        si.iden = i;
        si.up = false;
        si.down = false;
        si.steering = 0.0f;
        si.posX = 7*i;
        si.posY = 0.23f;
        si.posZ = 0.0f;
        si.rotX = 0.0f;
        si.rotY = 0.0f;
        si.rotZ = 0.0f;
        si.rotAngle = 1.0f;
        si.shoot = false;
        /*si.avX = 0.0f;
        si.avY = 0.0f;
        si.avZ = 0.0f;
        si.lvX = 0.0f;
        si.lvY = 0.0f;
        si.lvZ = 0.0f;*/
      onlinePlayers.push_back(si);
    }

    int argc=2;
    char* argv[]={"OnlineSubscriberThread","--Ice.Config=icestorm.cfg"};

    ic = Ice::initialize(argc, argv);

    string key = "IceStormAdmin.TopicManager.Default";
    ObjectPrx basePrx = ic->propertyToProxy(key);

    TopicManagerPrx topic_mgr = TopicManagerPrx::checkedCast(basePrx);

    /*
    if (!base) {
        cerr << "property " << key << " not set." << endl;
        return 0;
    }
     */

    ObjectPtr servant = new StateI(&onlinePlayers, &_start);
    ObjectAdapterPtr adapter = ic->createObjectAdapter("State.Subscriber");
    base = adapter->addWithUUID(servant);

    try {
        topic = topic_mgr->retrieve("StateTopic");
        topic->subscribeAndGetPublisher(QoS(), base);
    }
    catch(const NoSuchTopic& e) {
        //cerr << ": " << e << " name: " << e.name << endl;
        //return EXIT_FAILURE;
    }

    adapter->activate();

}

OnlineSubscriberThread::~OnlineSubscriberThread(){
    topic->unsubscribe(base);
}

void OnlineSubscriberThread::run() {
    while(1) {
      //showOnlineInfo();
      IceUtil::ThreadControl::sleep(IceUtil::Time::seconds(0));
    }
}

std::vector<StateInfo> OnlineSubscriberThread::getOnlineInfo() {
    return onlinePlayers;
}

bool OnlineSubscriberThread::getStartInfo() {
  //std::cout << _start << "\n" << std::endl;
  return _start;
}

void OnlineSubscriberThread::showOnlineInfo() {
   for(int i = 0; i<_numPlayers; i++){
     cout << "Hilo: " << onlinePlayers[i].iden << " <-> "  << onlinePlayers[i].posX << " <-> "  << onlinePlayers[i].posY << " <-> "  << onlinePlayers[i].posZ << endl;
   }

}

