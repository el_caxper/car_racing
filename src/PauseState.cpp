#include "PauseState.h"
#include "MenuState.h"
template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void
PauseState::enter ()
{
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    GameManager::getSingletonPtr()->getTrack(1)->pause();
    createGUI();
    _exitGame = false;
}

void
PauseState::exit ()
{
    GameManager::getSingletonPtr()->getTrack(1)->play();
}

void
PauseState::pause ()
{
}

void
PauseState::resume ()
{
}

bool
PauseState::frameStarted
(const Ogre::FrameEvent& evt)
{
    return true;
}

bool
PauseState::frameEnded
(const Ogre::FrameEvent& evt)
{
    if (_exitGame)
        return false;

    return true;
}

void
PauseState::keyPressed
(const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_P) {
        CEGUI::WindowManager::getSingleton().destroyAllWindows();
        popState();
    }
}

void
PauseState::keyReleased
(const OIS::KeyEvent &e)
{
}

void
PauseState::mouseMoved
(const OIS::MouseEvent &e)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void
PauseState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void
PauseState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

void PauseState::povMoved(const OIS::JoyStickEvent &e, int pov) {

}

void PauseState::axisMoved(const OIS::JoyStickEvent &e, int axis, int joystick) {

}

void PauseState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {

}

void PauseState::buttonPressed(const OIS::JoyStickEvent &e, int button, int joystick) {

}

void PauseState::buttonReleased(const OIS::JoyStickEvent &e, int button, int joystick) {

}

PauseState*
PauseState::getSingletonPtr ()
{
    return msSingleton;
}

PauseState&
PauseState::getSingleton ()
{ 
    assert(msSingleton);
    return *msSingleton;
}


void PauseState::createGUI()
{
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");

    /*std::cout << "Numero animaciones: "<< CEGUI::AnimationManager::getSingleton().getNumAnimations() << std::endl;
    std::cout << "Numero instancias animaciones: "<< CEGUI::AnimationManager::getSingleton().getNumAnimationInstances() << std::endl;

    if(CEGUI::AnimationManager::getSingleton().getNumAnimations()<13 &&
            CEGUI::AnimationManager::getSingleton().getNumAnimationInstances()>4){
      CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella");
      CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella2");
      CEGUI::AnimationManager::getSingleton().destroyAnimation("ViajeMeteorito");
      CEGUI::AnimationManager::getSingleton().destroyAnimation("LoopRotateRight");
    }*/

    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","pause_menu");
    CEGUI::Window* configWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("menu_pausa.layout");
    CEGUI::Window* exitButton = configWin->getChild("ExitButton");
    exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                               CEGUI::Event::Subscriber(&PauseState::quit,
                                                        this));
    exitButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                               CEGUI::Event::Subscriber(&PauseState::onMouseEnters,
                                                        this));
    CEGUI::Window* playButton = configWin->getChild("VolverButton");
    playButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                               CEGUI::Event::Subscriber(&PauseState::returnGame,
                                                        this));
    playButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                               CEGUI::Event::Subscriber(&PauseState::onMouseEnters,
                                                        this));
    CEGUI::Window* menuButton = configWin->getChild("MenuButton");
    menuButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                               CEGUI::Event::Subscriber(&PauseState::returnMenu,
                                                        this));
    menuButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                               CEGUI::Event::Subscriber(&PauseState::onMouseEnters,
                                                        this));
    sheet->addChild(configWin);
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);
}

CEGUI::MouseButton PauseState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
    case OIS::MB_Left:
        ceguiId = CEGUI::LeftButton;
        break;
    case OIS::MB_Right:
        ceguiId = CEGUI::RightButton;
        break;
    case OIS::MB_Middle:
        ceguiId = CEGUI::MiddleButton;
        break;
    default:
        ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

bool PauseState::returnMenu(const CEGUI::EventArgs &e)
{
    GameManager::getSingletonPtr()->getEffect(0)->play();
    _exitGame = 0;
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    popState();
    //popState();
    changeState(MenuState::getSingletonPtr());
    return true;
}


bool PauseState::returnGame(const CEGUI::EventArgs &e)
{
    GameManager::getSingletonPtr()->getEffect(0)->play();
    CEGUI::WindowManager::getSingleton().destroyAllWindows();
    popState();
    return true;
}

bool PauseState::quit(const CEGUI::EventArgs &e)
{
    GameManager::getSingletonPtr()->getEffect(0)->play();
    _exitGame = 1;
    return true;
}

bool PauseState::onMouseEnters(const CEGUI::EventArgs& e)
{
    GameManager::getSingletonPtr()->getEffect(1)->play();
    return true;
}


