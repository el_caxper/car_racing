#include "PlayState.h"
#include "PauseState.h"
#include "MenuState.h"
#include "IntroState.h"

#include <OgreRenderTexture.h>
#include <OgreHardwarePixelBuffer.h>
#include <OgreRectangle2D.h>
#include <IceUtil/IceUtil.h>

#include <iomanip> // setprecision
#include <sstream> // stringstream

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void
PlayState::enter()
{

    iaCounterStrikeGlobalOffensive=0;

    m_spline = new Ogre::SimpleSpline();

    /*
    FILE *f;
    f=fopen("ia_points.txt","r");
    int a,b,c;
    while(((fscanf(f,"%d,%d,%d",&a,&b,&c))!=EOF)){
        m_spline->addPoint(Ogre::Vector3(a,b,c));
    }
    */

    interpTime = 60.0;

    // Read data from file
    std::ifstream conf("conf.txt");
    conf >> _mode;
    conf >> _control_1;
    conf >> _control_2;
    conf >> _music;
    conf >> _numCars;
    conf >> _id;
    conf.close();

    //Create gui
    createGUI();


    std::srand((unsigned)time(NULL));
    _timeCounter = new Ogre::Timer();
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");


    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.2, 0.2, 0.2));

    // Directional light
    Ogre::SceneNode* lightDirNode = _sceneMgr->createSceneNode("LightDirNode");
    Ogre::Light* light1 = _sceneMgr->createLight("Light1");
    light1->setType(Ogre::Light::LT_DIRECTIONAL);
    light1->setDirection(Ogre::Vector3(1,-1,0));
    lightDirNode->attachObject(light1);
    _sceneMgr->getRootSceneNode()->addChild(lightDirNode);

    // Point light
    Ogre::SceneNode* lightPointNode = _sceneMgr->createSceneNode("lightPointNode");
    Ogre::Light* light2 = _sceneMgr->createLight("Light2");
    light2->setType(Ogre::Light::LT_POINT);
    light2->setPosition(8, 8, -2);
    light2->setSpecularColour(0.9, 0.9, 0.9);
    light2->setDiffuseColour(0.9, 0.9, 0.9);
    lightPointNode->attachObject(light2);
    _sceneMgr->getRootSceneNode()->addChild(lightPointNode);

    // Bullet --> debugDrawer
    _debugDrawer = new OgreBulletCollisions::DebugDrawer();
    _debugDrawer->setDrawWireframe(true);
    Ogre::SceneNode *node = _sceneMgr->getRootSceneNode()->
            createChildSceneNode("debugNode", Ogre::Vector3::ZERO);
    node->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer));

    // Bullet --> world
    Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
                Ogre::Vector3 (-100, -100, -100),
                Ogre::Vector3 (100,  100,  100));
    Ogre::Vector3 gravity = Ogre::Vector3(0, -9.8, 0);

    _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,
                                                   worldBounds, gravity);
    _world->setDebugDrawer (_debugDrawer);


    // Create scene GUI
    createScene();

    _startTime =  _timeCounter->getMilliseconds();
    _pauseTime = 0;

    _inGame = true;
    _gameOver = false;
    _exitGame = false;

    // Create ray for finishing line
    _rayFinishLine=_sceneMgr->createRayQuery(
                Ogre::Ray(Ogre::Vector3(20,0.5,10),Ogre::Vector3(-1,0,0)),
                RAY_QUERY_CAR);

    _rayPass1 = _sceneMgr->createRayQuery(
                Ogre::Ray(Ogre::Vector3(125.29498,0.5,128.34904),Ogre::Vector3(1,0,0)),
                RAY_QUERY_CAR);


    //if(_music)
        GameManager::getSingletonPtr()->getTrack(0)->stop();


    collition = 0;
    timeCol = 15;
    frameCountShoot = 0;
    frameCountRaycast = 0;
    frameCountCollision=20;

    if (_mode == ONLINE){

        // Launch publisher
        op = new OnlinePublisher();

        // Launch subscriber
        ost = new OnlineSubscriberThread(_numCars);
        IceUtil::ThreadControl tc = ost->start();
        tc.detach();

        localCar = StateInfo();
        rot = vehicles[players[_id]->getVehicleId()]->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getOrientation();
        //lv = vehicles[players[_id]->getVehicleId()]->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getLinearVelocity();
        //av = vehicles[players[_id]->getVehicleId()]->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getAngularVelocity();
        localCar.iden = _id;
        localCar.up = false;
        localCar.down = false;
        localCar.steering = 0.0;
        localCar.posX = vehicles[players[_id]->getVehicleId()]->getPosition().x;
        localCar.posY = 0.23;
        localCar.posZ = 0.0;
        localCar.rotX = rot.getX();
        localCar.rotY = rot.getY();
        localCar.rotZ = rot.getZ();
        localCar.rotAngle = rot.getW();
        localCar.shoot = false;

        /*localCar.avX = av.getX();
      localCar.avY = av.getY();
      localCar.avZ = av.getZ();
      localCar.lvX = lv.getX();
      localCar.lvY = lv.getY();
      localCar.lvZ = lv.getZ();*/

        /*while (!ost->getStartInfo()){
            sleep(0);
        }*/

        op->getStatePrx()->setStart(true);
        //std::cout << "Envianto TRUE\n" << std::endl;
        //std::cout << "Recibido: " << ost->getStartInfo() << "\n" << std::endl;
    }

    hideLoadingPopUp();

    if(_mode!=MULTIPLAYER) createMiniMap();

    GameManager::getSingletonPtr()->getEffect(2)->play();

}


void
PlayState::exit()
{
    for (int i = 0;  i < _numCars; i++) {
        delete vehicles[i];
    }
    for (int i = 0;  i < 2; i++) {
        delete players[i];
    }

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _sceneMgr->destroyAllCameras();
    _sceneMgr->destroyAllEntities();
    _sceneMgr->destroyAllStaticGeometry();
    _sceneMgr->getRootSceneNode()->removeAndDestroyAllChildren() ;

    if(_mode != MULTIPLAYER){
        delete mMiniScreen;
        Ogre::TextureManager::getSingleton().unloadAll(true);
        Ogre::TextureManager::getSingleton().removeAll();
        Ogre::MaterialManager::getSingleton().remove(renderMaterial->getHandle());
    }

    delete _world->getDebugDrawer();
    _world->setDebugDrawer(0);
    delete _world;

    std::cout<< "SALIENDO DEL JUEGO" << std::endl;

}

void
PlayState::pause()
{
    _inGame=false;
    _exitTime = _timeCounter->getMilliseconds();

}

void
PlayState::resume()
{
    _inGame=true;
    _pauseTime = _timeCounter->getMilliseconds() - _exitTime + _pauseTime;
    createGUI();
    hideLoadingPopUp();
    if(((_timeCounter->getMilliseconds() - _startTime - _pauseTime) / 1000)>3){
        _trafficLight->hide();
    }

}

bool
PlayState::frameStarted
(const Ogre::FrameEvent& evt)
{

    //iaCounterStrikeGlobalOffensive++;
    //if(iaCounterStrikeGlobalOffensive==30){

        /*Ogre::Vector3 pos = vehicles[1]->getPosition();
        float steer=vehicles[1]->getSteering();
        bool v_up=vehicles[1]->getUp();
        bool v_down=vehicles[1]->getDown();
        std::cout<<pos.x<<','<<pos.y<<','<<pos.z<<','<<v_up<<','<<v_down<<','<<steer<< std::endl;*/

        //iaCounterStrikeGlobalOffensive=0;
    //}


    if (!_gameOver){
        Ogre::Real deltaT = evt.timeSinceLastFrame;
        _world->stepSimulation(deltaT);
        int fps = 1.0 / deltaT;
        //printf("%d\n", fps);

        if(_mode==SINGLE_PLAYER){
            fTotalAppTime += deltaT;
            float fAlpha = (fTotalAppTime-3) / interpTime;
            if(fAlpha > 1.0f) fAlpha = 1.0f; // should be always greater than 0.0f
            if(fTotalAppTime > 3 && vehicles[1]->_die==false && fAlpha < 1) {
                /*
                vehicles[1]->rotateAndMoveTo(Ogre::Vector3(0,0,1).getRotationTo(
                                                 (m_spline->interpolate(fAlpha+deltaT)-m_spline->interpolate(fAlpha))),
                                             m_spline->interpolate(fAlpha));
                 */
            }
        }

        for (int i = 0; i < 2; i++){
            vehicles[i]->getRaycastVehicle()->applyEngineForce (0,0);
            vehicles[i]->getRaycastVehicle()->applyEngineForce (0,1);
        }

        //printf("%.3f", vehicles[0]->getPosition().y);

        //printf("FPS: %d\n", fps);


        if (_mode == ONLINE){

            rot = vehicles[players[_id]->getVehicleId()]->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getOrientation();
            //lv = vehicles[players[_id]->getVehicleId()]->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getLinearVelocity();
            //av = vehicles[players[_id]->getVehicleId()]->getRaycastVehicle()->getBulletVehicle()->getRigidBody()->getAngularVelocity();
            localCar.up = vehicles[players[_id]->getVehicleId()]->getUp();
            localCar.down = vehicles[players[_id]->getVehicleId()]->getDown();
            localCar.steering = vehicles[players[_id]->getVehicleId()]->getSteering();
            localCar.posX = vehicles[players[_id]->getVehicleId()]->getPosition().x;
            localCar.posY = vehicles[players[_id]->getVehicleId()]->getPosition().y - 1.57;
            localCar.posZ = vehicles[players[_id]->getVehicleId()]->getPosition().z;
            localCar.rotX = rot.getX();
            localCar.rotY = rot.getY();
            localCar.rotZ = rot.getZ();
            localCar.rotAngle = rot.getW();
            localCar.shoot = vehicles[players[_id]->getVehicleId()]->getShoot();
            /*localCar.avX = av.getX();
      localCar.avY = av.getY();
      localCar.avZ = av.getZ();
      localCar.lvX = lv.getX();
      localCar.lvY = lv.getY();
      localCar.lvZ = lv.getZ();*/
            op->getStatePrx()->sendState(localCar);
        }

        if (_mode == ONLINE){
            onlineVector = ost->getOnlineInfo();
        }

	updateTime();

        moveCars();

        setShoots();

        checkCollisions();

        _timeCount += 1;

        for (int i = 0; i < 2 ; i++){
            if(vehicles[i]!=NULL){
                vehicles[i]->getCamera()->setPosition(vehicles[players[i]->getVehicleId()]->getCameraNode()->_getDerivedPosition());
                vehicles[i]->getCamera()->lookAt(vehicles[players[i]->getVehicleId()]->getPosition());
            }
        }

        checkFinishLine();
        checkPass1();


        frameCountShoot += 1;
        frameCountRaycast +=1;
    }

    return true;
}

bool
PlayState::frameEnded
(const Ogre::FrameEvent& evt)
{
    Ogre::Real deltaT = evt.timeSinceLastFrame;
    _world->stepSimulation(deltaT);

    if (_exitGame)
        return false;

    return true;
}

void
PlayState::keyPressed
(const OIS::KeyEvent &e)
{
    if(_inGame) {
        int id = 0;
        bool mul = false;
        bool move = true;
        if (_control_1 == KEYBOARD_CONTROL && _mode == SINGLE_PLAYER) {
            id = 0;
        } else if (_control_1 == KEYBOARD_CONTROL && _mode == ONLINE) {
            id = _id;
        } else if (_control_1 == KEYBOARD_CONTROL && _control_2 == KEYBOARD_CONTROL && _mode == MULTIPLAYER) {
            mul = true;
            id = 0;
        } else if (_mode == MULTIPLAYER && _control_2 == KEYBOARD_CONTROL) {
            id = 1;
        } else if (_control_1 == KEYBOARD_CONTROL && _mode == MULTIPLAYER) {
            id = 0;
        }else {
            move = false;
        }
        if (e.key == OIS::KC_P) {
            pauseGame();
        }

        if (e.key == OIS::KC_H) {
            vehicles[id]->horn();
        }

        /*if (e.key == OIS::KC_U) {
            ost->showOnlineInfo();
        }*/

        /*if (e.key == OIS::KC_M) {
            vehicles[0]->moveTo(Ogre::Vector3(3, 0, 8));
            //printf("POS: id= %d   %.2f  %.2f  %.2f\n",vehicles[0]->getId(), vehicles[0]->getPosition().x, vehicles[0]->getPosition().y, vehicles[0]->getPosition().z);
        }*/
        /*if (e.key == OIS::KC_R) {
            vehicles[0]->rotate(Ogre::Radian(Ogre::Degree(-10)));
        }*/
        if (e.key == OIS::KC_F) {
            if (_mode == ONLINE){
                vehicles[players[_id]->getVehicleId()]->setShoot(true);
            } else{
                vehicles[players[1]->getVehicleId()]->setShoot(true);
            }
        }
        if (e.key == OIS::KC_L) {
            if (_mode == ONLINE){
                vehicles[players[_id]->getVehicleId()]->setShoot(true);
            } else{
                vehicles[players[0]->getVehicleId()]->setShoot(true);
                //vehicles[players[1]->getVehicleId()]->setShoot(true);
            }
        }
        if(e.key == OIS::KC_R){
            vehicles[players[1]->getVehicleId()]->horn();
        }
        if(e.key == OIS::KC_K){
            vehicles[players[0]->getVehicleId()]->horn();
        }
        if (move) {
            if (e.key == OIS::KC_UP) {
                vehicles[id]->setUp(true);
            }

            if (e.key == OIS::KC_DOWN) {
                vehicles[id]->setDown(true);
            }


            if (e.key == OIS::KC_LEFT) {
                vehicles[id]->setLeft(true);
            }

            if (e.key == OIS::KC_RIGHT) {
                vehicles[id]->setRight(true);
            }

            if (mul) {
                if (e.key == OIS::KC_W) {
                    vehicles[1]->setUp(true);
                }

                if (e.key == OIS::KC_S) {
                    vehicles[1]->setDown(true);
                }

                if (e.key == OIS::KC_A) {
                    vehicles[1]->setLeft(true);
                }

                if (e.key == OIS::KC_D) {
                    vehicles[1]->setRight(true);
                }
            }
        }

    }else {
        CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
        CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);
    }



}

void
PlayState::keyReleased
(const OIS::KeyEvent &e)
{
    int id = 0;
    bool mul = false;
    bool move = true;
    if (_control_1 == KEYBOARD_CONTROL && _mode == SINGLE_PLAYER) {
        id = 0;
    } else if (_control_1 == KEYBOARD_CONTROL && _mode == ONLINE) {
        id = _id;
    } else if (_control_1 == KEYBOARD_CONTROL && _control_2 == KEYBOARD_CONTROL && _mode == MULTIPLAYER) {
        mul = true;
        id = 0;
    } else if ( _mode == MULTIPLAYER && _control_2 == KEYBOARD_CONTROL) {
        id = 1;
    } else if (_control_1 == KEYBOARD_CONTROL && _mode == MULTIPLAYER) {
        id = 0;
    }else {
        move = false;
    }
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }

    if (move) {
        if (e.key == OIS::KC_UP) {
            vehicles[id]->setUp(false);
        }

        if (e.key == OIS::KC_DOWN) {
            vehicles[id]->setDown(false);
        }

        if (e.key == OIS::KC_LEFT) {
            vehicles[id]->setLeft(false);
        }

        if (e.key == OIS::KC_RIGHT) {
            vehicles[id]->setRight(false);
        }

        if (mul) {
            if (e.key == OIS::KC_W) {
                vehicles[1]->setUp(false);
            }

            if (e.key == OIS::KC_S) {
                vehicles[1]->setDown(false);
            }

            if (e.key == OIS::KC_A) {
                vehicles[1]->setLeft(false);
            }

            if (e.key == OIS::KC_D) {
                vehicles[1]->setRight(false);
            }
        }
    }
}

void
PlayState::mouseMoved
(const OIS::MouseEvent &e)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void
PlayState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void
PlayState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

void PlayState::povMoved(const OIS::JoyStickEvent &e, int pov) {
    //std::cout << "Pov: " << pov << std::endl;
}

void PlayState::axisMoved(const OIS::JoyStickEvent &e, int axis, int joystick) {
    int id = 0;
    /*
    Ogre::Vector3 pos = vehicles[1]->getPosition();
    float steer=vehicles[1]->getSteering();
    bool v_up=vehicles[1]->getUp();
    bool v_down=vehicles[1]->getDown();
    std::cout<<pos.x<<','<<pos.y<<','<<pos.z<<','<<v_up<<','<<v_down<<','<<steer<< std::endl;
    */
    bool move = true;
    if ( _control_1 == JOYSTICK_CONTROL && _mode == SINGLE_PLAYER && joystick == 0){
        id = 0;
    } else if (_control_1 == JOYSTICK_CONTROL && _mode == ONLINE && joystick == 0){
        id = _id;
    }else if (_control_1 == JOYSTICK_CONTROL && _mode == MULTIPLAYER && joystick == 0){
        id = 0;
    }  else if (_control_1 == JOYSTICK_CONTROL && _mode == MULTIPLAYER && _control_2 == JOYSTICK_CONTROL && joystick == 1){
        id = 1;
    } else if (_control_1 != JOYSTICK_CONTROL && _mode == MULTIPLAYER && _control_2 == JOYSTICK_CONTROL && joystick == 0){
        id = 1;
    } else {
        move = false;
    }

    if (move) {
        double normalizedValue = ((e.state.mAxes[0].abs) - (-32768.0)) / (32767.0 - (-32768.0));
        if (axis == 0) {
            if (normalizedValue > 0.7) {
                double axisMove = (normalizedValue - 0.7) / (1.0 - 0.7) * 0.15;
                vehicles[id]->setSteering(-axisMove);
                vehicles[id]->getRaycastVehicle()->setSteeringValue(vehicles[id]->getSteering(), 0);
                vehicles[id]->getRaycastVehicle()->setSteeringValue(vehicles[id]->getSteering(), 1);
            } else if (normalizedValue < 0.4) {
                double axisMove = normalizedValue / 0.4 * 0.15;
                vehicles[id]->setSteering(0.25 - axisMove);
                vehicles[id]->getRaycastVehicle()->setSteeringValue(vehicles[id]->getSteering(), 0);
                vehicles[id]->getRaycastVehicle()->setSteeringValue(vehicles[id]->getSteering(), 1);
            } else {
                vehicles[id]->setSteering(0.0);
                vehicles[id]->getRaycastVehicle()->setSteeringValue(vehicles[id]->getSteering(), 0);
                vehicles[id]->getRaycastVehicle()->setSteeringValue(vehicles[id]->getSteering(), 1);
            }
        }
    }


}

void PlayState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {
    //std::cout << "SliderID: " << sliderID << std::endl;
}

void PlayState::buttonPressed(const OIS::JoyStickEvent &e, int button, int joystick) {
    //std::cout << "Boton: " << e.state.mAxes[1].abs << std::endl;
    int id = 0;
    /*
    Ogre::Vector3 pos = vehicles[1]->getPosition();
    float steer=vehicles[1]->getSteering();
    bool v_up=vehicles[1]->getUp();
    bool v_down=vehicles[1]->getDown();
    std::cout<<pos.x<<','<<pos.y<<','<<pos.z<<','<<v_up<<','<<v_down<<','<<steer<< std::endl;
    */
    bool move = true;
    if ( _control_1 == JOYSTICK_CONTROL && _mode == SINGLE_PLAYER && joystick == 0){
        id = 0;
    } else if (_control_1 == JOYSTICK_CONTROL && _mode == ONLINE && joystick == 0){
        id = _id;
    } else if (_control_1 == JOYSTICK_CONTROL && _mode == MULTIPLAYER && joystick == 0){
        id = 0;
    }  else if (_control_1 == JOYSTICK_CONTROL && _mode == MULTIPLAYER && _control_2 == JOYSTICK_CONTROL && joystick == 1){
        id = 1;
    } else if (_control_1 != JOYSTICK_CONTROL && _mode == MULTIPLAYER && _control_2 == JOYSTICK_CONTROL && joystick == 0){
        id = 1;
    } else {
        move = false;
    }
    if (move){
        if (_inGame) {
            if (button == 9) {
                pauseGame();
            }
            if (button == 2) {
                vehicles[id]->setUp(true);
            }

            if (button == 3) {
                vehicles[id]->setDown(true);
            }

            if (button == 7) {
                vehicles[id]->setShoot(true);
            }
            if (button == 6) {
                vehicles[id]->horn();
            }

        } else {
            //CEGUI::System::getSingleton()->getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
            //CEGUI::System::getSingleton()->getDefaultGUIContext().injectChar(e.text);
        }
    }
}

void PlayState::buttonReleased(const OIS::JoyStickEvent &e, int button, int joystick) {
    int id = 0;
    bool move = true;
    if ( _control_1 == JOYSTICK_CONTROL && _mode == SINGLE_PLAYER && joystick == 0){
        id = 0;
    } else if (_control_1 == JOYSTICK_CONTROL && _mode == ONLINE && joystick == 0){
        id = _id;
    }else if (_control_1 == JOYSTICK_CONTROL && _mode == MULTIPLAYER && joystick == 0){
        id = 0;
    }  else if (_control_1 == JOYSTICK_CONTROL && _mode == MULTIPLAYER && _control_2 == JOYSTICK_CONTROL && joystick == 1){
        id = 1;
    } else if (_control_1 != JOYSTICK_CONTROL && _mode == MULTIPLAYER && _control_2 == JOYSTICK_CONTROL && joystick == 0){
        id = 1;
    } else {
        move = false;
    }
    if (move){
        if (_inGame) {
            if (button == 9) {
                pauseGame();
            }
            if (button == 2) {
                vehicles[id]->setUp(false);
            }

            if (button == 3) {
                vehicles[id]->setDown(false);
            }
        }
    }
}

PlayState*
PlayState::getSingletonPtr ()
{
    return msSingleton;
}

PlayState&
PlayState::getSingleton ()
{ 
    assert(msSingleton);
    return *msSingleton;
}

CEGUI::MouseButton PlayState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
    case OIS::MB_Left:
        ceguiId = CEGUI::LeftButton;
        break;
    case OIS::MB_Right:
        ceguiId = CEGUI::RightButton;
        break;
    case OIS::MB_Middle:
        ceguiId = CEGUI::MiddleButton;
        break;
    default:
        ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}



void PlayState::createGUI()
{
    using namespace CEGUI;


    /*AnimationManager& animMgr = AnimationManager::getSingleton();
    animMgr.loadAnimationsFromXML("GameMenu.anims");*/

    /*CEGUI::Window* */ sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","timeIndicator");
    /*CEGUI::Window* */configWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("pantalla_juego.layout");
    CEGUI::Window* pauseButton = configWin->getChild("FrameWindow")->getChild("PausaButton");
    pauseButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                                CEGUI::Event::Subscriber(&PlayState::pauseGame,
                                                         this));


    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();
    sheet->addChild(configWin);

    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);
    _textBoxTime = static_cast<DefaultWindow*>(configWin->getChild("TopBar")->getChild("Tiempo"));

    _loadingWindow = configWin->getChild("Loading");
    _trafficLight = configWin->getChild("traffic_light");
    _speedometer = configWin->getChild("Speedometer");
    _speedometerAguja = _speedometer->getChild("aguja");
    _losePopup = configWin->getChild("Eliminated");

    if(_mode==MULTIPLAYER) _speedometer->hide();

    /*CEGUI::Window* d_navigationTravelIcon = configWin->getChild("Loading")->getChild("Image_load");

    travelRotateInInst = CEGUI::AnimationManager::getSingleton().instantiateAnimation("TravelRotateIn");
    travelRotateInInst->setTargetWindow(d_navigationTravelIcon);
    */
}

void PlayState::createScene(){

    using namespace OgreBulletCollisions;
    using namespace OgreBulletDynamics;


    Ogre::Entity * entity = _sceneMgr->createEntity("road_protection","road_protection.mesh");
    Ogre::SceneNode *node = _sceneMgr->createSceneNode("road_protection");
    node->attachObject(entity);
    entity->setQueryFlags(RAY_QUERY_NONE);

    _sceneMgr->getRootSceneNode()->addChild(node);
    OgreBulletCollisions::StaticMeshToShapeConverter *trimeshConverter = new OgreBulletCollisions::StaticMeshToShapeConverter(entity);

    trackTrimesh = trimeshConverter->createTrimesh();

    rigidTrack = new OgreBulletDynamics::RigidBody("road_protection", _world);
    rigidTrack->setStaticShape(node, trackTrimesh, 0.8, 0.95, Ogre::Vector3::ZERO,
                               Ogre::Quaternion::IDENTITY);

    rigidTrack->disableDeactivation();

    entity = _sceneMgr->createEntity("track","road.mesh");
    node = _sceneMgr->createSceneNode("track");
    node->attachObject(entity);
    entity->setQueryFlags(RAY_QUERY_NONE);

    _sceneMgr->getRootSceneNode()->addChild(node);
    trimeshConverter = new
            OgreBulletCollisions::StaticMeshToShapeConverter(entity);

    trackTrimesh = trimeshConverter->createTrimesh();

    rigidTrack = new OgreBulletDynamics::RigidBody("track", _world);
    rigidTrack->setStaticShape(node, trackTrimesh, 0.8, 0.95, Ogre::Vector3::ZERO,
                               Ogre::Quaternion::IDENTITY);

    rigidTrack->disableDeactivation();




    delete trimeshConverter;

    Ogre::Entity *entityFinish = _sceneMgr->createEntity("Finish_line_flag.mesh");

    Ogre::SceneNode *nodeFinish = _sceneMgr->createSceneNode("finishLine");
    nodeFinish->attachObject(entityFinish);

    _sceneMgr->getRootSceneNode()->addChild(nodeFinish);


    // Plano
    Ogre::Plane plane1(Ogre::Vector3(0,0.1,0), 0);
    Ogre::MeshManager::getSingleton().createPlane("p1",
                                                  Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane1,
                                                  2500, 2500, 1, 1, true, 1, 20, 20, Ogre::Vector3::UNIT_Z);
    Ogre::SceneNode* node_plane = _sceneMgr->createSceneNode("ground");
    Ogre::Entity* groundEnt = _sceneMgr->createEntity("planeEnt", "p1");
    groundEnt->setMaterialName("GrassFloor");
    node_plane->attachObject(groundEnt);
    _sceneMgr->getRootSceneNode()->addChild(node_plane);
    node_plane->setPosition(Ogre::Vector3(0,-0.2,0));



    // Creamos los vehiculos =============================================
    for (int i = 0; i < _numCars ; i++){
        vehicles[i] = new Car(_sceneMgr,_world, Ogre::Vector3(7*i,0.23,0), &frameCountShoot);

    }

    // Creamos los jugadores
    int controlPlayer=-1;
    for (int i = 0; i < 2 ; i++){
        if (_mode == ONLINE && i == _id){
            controlPlayer=_control_1;
        } else if (_mode == ONLINE && i != _id){
            controlPlayer=KEYBOARD_CONTROL;
        }
        else{
            if(i==0) controlPlayer=_control_1;
            if(i==1) {
                if (_mode == MULTIPLAYER) {
                    controlPlayer = _control_2;
                } else {
                    controlPlayer = IA_CONTROL;
                }
            }
        }
        players[i] = new Player(i, controlPlayer);
    }

    if(_mode==SINGLE_PLAYER ){
        _viewport = _root->getAutoCreatedWindow()->addViewport(vehicles[players[0]->getVehicleId()]->getCamera());
        _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));
        _viewport->setVisibilityMask(RAY_QUERY_CAR);

    }else if (_mode == ONLINE) {
        _viewport = _root->getAutoCreatedWindow()->addViewport(vehicles[players[_id]->getVehicleId()]->getCamera());
        _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));
        _viewport->setVisibilityMask(RAY_QUERY_CAR);

    } else{
        _viewport = _root->getAutoCreatedWindow()->addViewport(vehicles[players[1]->getVehicleId()]->getCamera(), 0, 0, 0, 0.5, 1);
        _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));
        vehicles[players[1]->getVehicleId()]->getCamera()->setAspectRatio(Ogre::Real(_viewport->getActualWidth()) / Ogre::Real(_viewport->getActualHeight()));
        _viewport->setVisibilityMask(RAY_QUERY_CAR);

        _viewport = _root->getAutoCreatedWindow()->addViewport(vehicles[players[0]->getVehicleId()]->getCamera(), 1, 0.5, 0, 0.5, 1);
        _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));
        vehicles[players[0]->getVehicleId()]->getCamera()->setAspectRatio(Ogre::Real(_viewport->getActualWidth()) / Ogre::Real(_viewport->getActualHeight()));
        _viewport->setVisibilityMask(RAY_QUERY_CAR);

    }

    _sceneMgr->setSkyBox(true, "Sky_box/CloudySky",105000);

}

void PlayState::pauseGame(){

    pushState(PauseState::getSingletonPtr());

}

void PlayState::updateTime(){

    std::stringstream saux;
    saux << "Time "<< (_timeCounter->getMilliseconds() - _startTime - _pauseTime) / 1000;
    _textBoxTime->setText(std::to_string((_timeCounter->getMilliseconds() - _startTime - _pauseTime) / 1000));

    if(((_timeCounter->getMilliseconds() - _startTime - _pauseTime) / 1000)<1){
        if(_timeCounter->getMilliseconds()>100 && _timeCounter->getMilliseconds() <300){
            for (int i = 0; i < _numCars; ++i) {
                vehicles[i]->setState(Car::DESACTIVE);
            }
        }

    }else if(((_timeCounter->getMilliseconds() - _startTime - _pauseTime) / 1000)<2){
        _trafficLight->setProperty("Image", "traffic_light/amber");

    }else if(((_timeCounter->getMilliseconds() - _startTime - _pauseTime) / 1000)<3){

        _trafficLight->setProperty("Image", "traffic_light/green");

    }else if(((_timeCounter->getMilliseconds() - _startTime - _pauseTime) / 1000)<4){
        _trafficLight->hide();

        if(_music)
            if(!GameManager::getSingletonPtr()->getTrack(1)->isPlaying())
                GameManager::getSingletonPtr()->getTrack(1)->play();

        for (int i = 0; i < _numCars; ++i) {
            vehicles[i]->setState(Car::ACTIVE);
        }
    }

    /*if((int)((_timeCounter->getMilliseconds() - _startTime - _pauseTime) / 1000) >
            (int)(((_timeCounter->getMilliseconds() - _startTime - _pauseTime-500) / 1000))){

    }*/

    if(_mode!=MULTIPLAYER) updateSpeedometer();

}


void PlayState::showEnd(int idWinner){

    _inGame = false;

    _losePopup->hide();
    /*std::cout << "Num Animaciones PLAY" <<CEGUI::AnimationManager::getSingleton().getNumAnimations()<< std::endl;
    std::cout << "Num Inst Animaciones PLAY" <<CEGUI::AnimationManager::getSingleton().getNumAnimationInstances()<< std::endl;

    if(CEGUI::AnimationManager::getSingleton().getNumAnimations()<13 &&
            CEGUI::AnimationManager::getSingleton().getNumAnimationInstances()>4){
        CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella");
        CEGUI::AnimationManager::getSingleton().destroyAnimation("ParpadeoEstrella2");
        CEGUI::AnimationManager::getSingleton().destroyAnimation("ViajeMeteorito");
        CEGUI::AnimationManager::getSingleton().destroyAnimation("LoopRotateRight");
    }*/

    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","endGame");

    CEGUI::Window* configWin = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("fin_juego.layout");
    CEGUI::Window* exitButton = configWin->getChild("ExitButton");
    exitButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                               CEGUI::Event::Subscriber(&PlayState::returnMenu,
                                                        this));
    exitButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                               CEGUI::Event::Subscriber(&PlayState::onMouseEnters,
                                                        this));

    CEGUI::Window* labelResult = configWin->getChild("FinJuego");
    CEGUI::Window* labelName = configWin->getChild("NameLabel");
    _editName = configWin->getChild("NameBox");

    CEGUI::Window* saveButton = configWin->getChild("GuardarButton");
    saveButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                               CEGUI::Event::Subscriber(&PlayState::saveRecord,
                                                        this));
    saveButton->subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
                               CEGUI::Event::Subscriber(&PlayState::onMouseEnters,
                                                        this));



    if(idWinner){
        GameManager::getSingletonPtr()->getTrack(1)->stop();
        GameManager::getSingletonPtr()->getEffect(6)->play();
        labelResult->setText(std::string("GANADOR: COCHE")+std::to_string(idWinner));
        _endTime = (int)(_timeCounter->getMilliseconds() - _startTime - _pauseTime) / 1000;

       if(_mode == SINGLE_PLAYER && idWinner!=1 ){
          labelResult->setText("SIGUE PRACTICANDO");
          _editName->setVisible(false);
          labelName->setVisible(false);
          saveButton->setVisible(false);
        
       }
    }/*else{
        //GameManager::getSingletonPtr()->getTrack(1)->stop();
//        GameManager::getSingletonPtr()->getEffect(3)->play();
        labelResult->setText("PERDEDOR");
        _editName->setVisible(false);
        labelName->setVisible(false);
        saveButton->setVisible(false);
    }*/

    sheet->addChild(configWin);
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);

}

bool PlayState::returnMenu()
{
    _exitGame = 0;
    changeState(MenuState::getSingletonPtr());
    return true;
}

void PlayState::saveRecord(const CEGUI::EventArgs &e){

    if(_editName->getText().length()>0){

        YAML::Node doc = YAML::LoadFile("./records.yaml");

        YAML::Emitter out;
        out << YAML::BeginMap;


        bool recordInserted = false;

        if(doc.size()>0){

            for (YAML::const_iterator it = doc.begin(); it != doc.end(); ++it) {
                YAML::Node key = it->first;

                if(std::stoi(doc[key][0].as<std::string>()) > _endTime && !recordInserted){
                    out << YAML::Key << _editName->getText().c_str();
                    out << YAML::Value << YAML::BeginSeq <<  _endTime << YAML::EndSeq;
                    out << YAML::Key << key.as<std::string>();
                    out << YAML::Value << YAML::BeginSeq << doc[key][0].as<std::string>()  << YAML::EndSeq;
                    recordInserted = true;
                }else if (std::stoi(doc[key][0].as<std::string>()) == _endTime && !recordInserted){

                    out << YAML::Key << _editName->getText().c_str();
                    out << YAML::Value << YAML::BeginSeq <<  _endTime << YAML::EndSeq;
                    out << YAML::Key << key.as<std::string>();
                    out << YAML::Value << YAML::BeginSeq << doc[key][0].as<std::string>()  << YAML::EndSeq;
                    recordInserted = true;

                }else {
                    out << YAML::Key << key.as<std::string>();
                    out << YAML::Value << YAML::BeginSeq << doc[key][0].as<std::string>()  << YAML::EndSeq;
                }
            }

            if(!recordInserted){
                out << YAML::Key << _editName->getText().c_str();
                out << YAML::Value << YAML::BeginSeq <<  _endTime << YAML::EndSeq;
            }

        }else {
            out << YAML::Key << _editName->getText().c_str();
            out << YAML::Value << YAML::BeginSeq <<  _endTime << YAML::EndSeq;
        }

        out << YAML::EndMap;

        std::ofstream fout("./records.yaml");
        fout << out.c_str();

        returnMenu();
    }

}

bool PlayState::onMouseEnters(const CEGUI::EventArgs& e)
{
    GameManager::getSingletonPtr()->getEffect(1)->play();
    return true;
}

void PlayState::moveCars() {

    for (int i = 0; i < 2; i++){
        if (_mode == ONLINE && i != _id){
            vehicles[players[i]->getVehicleId()]->moveOnline(onlineVector.at(i));
        }
        vehicles[players[i]->getVehicleId()]->move(players[i]);
    }
}


void PlayState::checkCollisions() {
    btCollisionWorld *collisionWorld = _world->getBulletCollisionWorld();
    int numManifolds = collisionWorld->getDispatcher()->getNumManifolds();

    for (int i=0;i<numManifolds;i++)
    {
        btPersistentManifold* contactManifold =  collisionWorld->getDispatcher()->getManifoldByIndexInternal(i);
        btCollisionObject* btCA = (btCollisionObject *)(contactManifold->getBody0());
        btCollisionObject* btCB = (btCollisionObject *)(contactManifold->getBody1());

        OgreBulletCollisions::Object *A = _world->findObject(btCA);
        OgreBulletCollisions::Object *B = _world->findObject(btCB);
        Ogre::SceneNode * node1 = A->getRootNode();
        Ogre::SceneNode * node2 = B->getRootNode();

        if( node1->getName().find("chassis") != std::string::npos && node2->getName().find("chassis") != std::string::npos){
            if(frameCountCollision==20){
                GameManager::getSingletonPtr()->getEffect(7)->play();
                //printf("hoal\n");
                frameCountCollision=0;
            }
            else{
                frameCountCollision++;
            }

        }

        if( node2->getName().find("Ball") != std::string::npos && !node1->getName().compare(_sceneMgr->getSceneNode("track")->getName())){
            Ogre::SceneNode* node = NULL;
            // std::cout << "Eliminando bola " << B->getName() << std::endl;
            node = B->getRootNode(); delete B;
            _sceneMgr->getRootSceneNode()->removeAndDestroyChild (node->getName());
        }else if(node1->getName().find("Ball") != std::string::npos && !node2->getName().compare(_sceneMgr->getSceneNode("track")->getName())){
            Ogre::SceneNode* node = NULL;
            //std::cout << "Eliminando bola " << A->getName() << std::endl;
            node = A->getRootNode(); delete A;
            _sceneMgr->getRootSceneNode()->removeAndDestroyChild (node->getName());
        }else if(node1->getName().find("Ball") != std::string::npos && node2->getName().find("chassis") != std::string::npos){
            //std::cout << "Colision BOLA-CHASIS" << A->getName() << std::endl;
            Ogre::SceneNode* node = NULL;
            node = A->getRootNode(); delete A;
            _sceneMgr->getRootSceneNode()->removeAndDestroyChild (node->getName());

            --vehicles[B->getName()[B->getName().size() - 1] - '0'-1]->carLifes;
            if(vehicles[B->getName()[B->getName().size() - 1] - '0'-1]->carLifes<=0){
                vehicles[B->getName()[B->getName().size() - 1] - '0'-1]->hide();
                _world->getBulletCollisionWorld()->removeCollisionObject(B->getBulletObject());
                //std::cout << "Coche " << B->getName()[B->getName().size() - 1] - '0'-1 << " eliminado" << std::endl;
                if(A->getName()[A->getName().size() - 1] - '0'-1<2){
                    if(_mode==MULTIPLAYER)
                        _losePopup->show();
		    /*else if(_mode==ONLINE && _id == A->getName()[A->getName().size() - 1] - '0'-1){
                        _losePopup->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0,0)));
                        _losePopup->show();
                    }*/

                }
            }
        }else if(node2->getName().find("Ball") != std::string::npos && node1->getName().find("chassis") != std::string::npos){
            //std::cout << "Colision "<< B->getName() << " - " << A->getName() << std::endl;
            Ogre::SceneNode* node = NULL;
            node = B->getRootNode(); delete B;
            _sceneMgr->getRootSceneNode()->removeAndDestroyChild (node->getName());

            --vehicles[A->getName()[A->getName().size() - 1] - '0'-1]->carLifes;
            if(vehicles[A->getName()[A->getName().size() - 1] - '0'-1]->carLifes<=0){
                vehicles[A->getName()[A->getName().size() - 1] - '0'-1]->hide();
                _world->getBulletCollisionWorld()->removeCollisionObject(A->getBulletObject());
                std::cout << "Coche " << A->getName()[A->getName().size() - 1] - '0'-1 << " eliminado" << std::endl;
                if(A->getName()[A->getName().size() - 1] - '0'-1<2){
                    if(_mode==MULTIPLAYER){
                        if (A->getName()[A->getName().size() - 1] - '0'-1 == 0){
                          _losePopup->setPosition(CEGUI::UVector2(CEGUI::UDim(0.3,0),CEGUI::UDim(0,0)));
                        } else{
                          _losePopup->setPosition(CEGUI::UVector2(CEGUI::UDim(-0.25,0),CEGUI::UDim(0,0)));
                        }
                        _losePopup->show();
                    }/*else if(_mode==ONLINE && _id == (A->getName()[A->getName().size() - 1] - '0'-1)){
                        _losePopup->setPosition(CEGUI::UVector2(CEGUI::UDim(0,0),CEGUI::UDim(0,0)));
                        _losePopup->show();
                     }*/

                }

            }

        }
    }

}
void PlayState::hideLoadingPopUp(){

    _loadingWindow->hide();

}


void PlayState::createMiniMap(){

    Ogre::TexturePtr rttTexture =
            Ogre::TextureManager::getSingleton().createManual(
                "RttTex",
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
                Ogre::TEX_TYPE_2D,
                512, 512,
                0,
                Ogre::PF_R8G8B8,
                Ogre::TU_RENDERTARGET);

    renderTextureMM = rttTexture->getBuffer()->getRenderTarget();

    _mmCamera = _sceneMgr->createCamera("minimap_cam");
    _mmCamera->setPosition(Ogre::Vector3(225,1900,-50));
    _mmCamera->lookAt(Ogre::Vector3(225,0,50));
    _mmCamera->setNearClipDistance(1000);

    renderTextureMM->addViewport(_mmCamera);
    renderTextureMM->getViewport(0)->setSkiesEnabled(false);
    renderTextureMM->getViewport(0)->setAutoUpdated(true);
    renderTextureMM->getViewport(0)->setClearEveryFrame(true);
    renderTextureMM->getViewport(0)->setBackgroundColour(Ogre::ColourValue::Black);
    renderTextureMM->getViewport(0)->setOverlaysEnabled(false);
    renderTextureMM->getViewport(0)->setVisibilityMask(CAR_BALL_MM);

    _mmCamera->setAspectRatio(Ogre::Real(renderTextureMM->getViewport(0)->getActualWidth()) / Ogre::Real(renderTextureMM->getViewport(0)->getActualHeight()));
    renderTextureMM->update();
    //    renderTexture->writeContentsToFile("start.png");


    mMiniScreen = new Ogre::Rectangle2D(true);
    mMiniScreen->setCorners(.6, 1.0, 1.0, .3);
    mMiniScreen->setBoundingBox(Ogre::AxisAlignedBox::BOX_INFINITE);

    miniScreenNode = _sceneMgr->createSceneNode("miniScreenNode");
    mMiniScreen->setVisibilityFlags(RAY_QUERY_CAR);
    miniScreenNode->attachObject(mMiniScreen);
    _sceneMgr->getRootSceneNode()->addChild(miniScreenNode);

    renderMaterial =
            Ogre::MaterialManager::getSingleton().create(
                "RttMat",
                Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

    renderMaterial->getTechnique(0)->getPass(0)->setLightingEnabled(false);
    renderMaterial->getTechnique(0)->getPass(0)->createTextureUnitState("RttTex");

    mMiniScreen->setMaterial("RttMat");

}

void PlayState::setShoots() {
    for (int i = 0; i < 2; i++){
        if (i != _id && _mode == ONLINE){
            vehicles[players[i]->getVehicleId()]->setShoot(onlineVector.at(i).shoot);
        }
        vehicles[players[i]->getVehicleId()]->fire();
    }

}


void PlayState::updateSpeedometer(){

    if(vehicles[0]->getSpeedKMHour()<0){
        _speedometerAguja->setRotation(CEGUI::Quaternion::axisAngleDegrees(CEGUI::Vector3f(0.0,0.0,1.0),-130));
        _speedometer->getChild("R")->show();
        _speedometer->getChild("Speed")->setText("0.0");

    }else{
        _speedometerAguja->setRotation(CEGUI::Quaternion::axisAngleDegrees(CEGUI::Vector3f(0.0,0.0,1.0),(float)(vehicles[0]->getSpeedKMHour()*1.35-130.0)));

        std::ostringstream out;
        out<<std::setprecision(1)<<std::fixed<<std::showpoint<< vehicles[0]->getSpeedKMHour()*1.35;

        _speedometer->getChild("Speed")->setText(out.str());
        _speedometer->getChild("R")->hide();

    }

}

void PlayState::checkFinishLine(){

    // Check finish line
    Ogre::RaySceneQueryResult &result = _rayFinishLine->execute();
    Ogre::RaySceneQueryResult::iterator it;
    it = result.begin();
    if (it != result.end()) {
        //if ((it->movable != NULL) && ((it->movable->getMovableType().compare("Entity") == 0)
        //                           ||it->movable->getMovableType().compare("ManualObject") == 0)) {
        std::string name_sel = it->movable->getParentSceneNode()->getName();
        Ogre::SceneNode *nodeaux = _sceneMgr->getSceneNode(name_sel);
        //std::cout << name_sel << std::endl;
        for (int i = 0; i < 2; i++) {
            if (vehicles[i]->getChassisSceneNode() == nodeaux && vehicles[i]->_die == false) {
                //printf("Coche %d en la meta\n", i+1);
                if(vehicles[i]->Checkpoint1 == true){
                    //printf("Coche %d ya ha pasado control 1\n", i+1);
		    _gameOver=true;
                    showEnd(vehicles[i]->getId());
                }
                break;
            }
        }
        //}
    }
}

void PlayState::checkPass1(){

    // Check finish line
    Ogre::RaySceneQueryResult &result = _rayPass1->execute();
    Ogre::RaySceneQueryResult::iterator it;
    it = result.begin();
    if (it != result.end()) {
        //if ((it->movable != NULL) && ((it->movable->getMovableType().compare("Entity") == 0)
        //                           ||it->movable->getMovableType().compare("ManualObject") == 0)) {
        std::string name_sel = it->movable->getParentSceneNode()->getName();
        Ogre::SceneNode *nodeaux = _sceneMgr->getSceneNode(name_sel);
        //std::cout << name_sel << std::endl;
        for (int i = 0; i < 2; i++) {
            if (vehicles[i]->getChassisSceneNode() == nodeaux
                    && vehicles[i]->_die == false && vehicles[i]->Checkpoint1 == false) {
                //printf("Coche %d Punto de control 1\n", i+1);
                vehicles[i]->Checkpoint1=true;
                break;
            }
        }
        //}
    }
}

