
#ifndef CAR_H
#define CAR_H

#include <Ogre.h>
#include <OGRE/Ogre.h>
#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/OgreBulletCollisions.h>
#include "OgreBullet/Dynamics/Constraints/OgreBulletDynamicsRaycastVehicle.h"
#include "OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h"
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include "Player.h"
#include "State.h"

using namespace Online;

#define RAY_QUERY_NONE 1 << 1
#define RAY_QUERY_CAR 1 << 2
#define CAR_BALL_MM 1 << 3

class Car{
    public:
        static int count;
        Car(Ogre::SceneManager*,OgreBulletDynamics::DynamicsWorld*, Ogre::Vector3, int* );
        //Car(const Car&);
        ~Car();
        Car();

        int getId();
        OgreBulletDynamics::RaycastVehicle* getRaycastVehicle();
        float getSteering();
        bool getUp();
        bool getDown();
        bool getLeft();
        bool getRight();
        bool getShoot();
        float getEngineForce();

        void setUp(bool);
        void setDown(bool);
        void setLeft(bool);
        void setRight(bool);
        void setShoot(bool);
        void setSteering(float);

        void move(Player*);
        void moveOnline(StateInfo);
        void moveTo(Ogre::Vector3);
        void rotateAndMoveTo(Ogre::Radian angle, Ogre::Vector3 pos);
        void rotateAndMoveTo(Ogre::Quaternion angle,Ogre::Vector3 pos);
        void rotate(Ogre::Degree);

        Ogre::Vector3 getPosition();
        Ogre::Camera* getCamera();
        Ogre::SceneNode* getCameraNode();

        void fire();
        void hide();
        void die();

        void horn();

        bool _die=false;
        float getSpeedKMHour();

        Ogre::SceneNode* getChassisSceneNode();
        bool Checkpoint1 = false;
        bool Checkpoint2 = false;

        int carLifes=5;


        enum carState{
          DESACTIVE = 0,
          ACTIVE
        };
        void setState(enum carState active);


    protected:

        int id;

        Ogre::SceneManager* _sceneMgr;
        OgreBulletDynamics::DynamicsWorld* _world;
        Ogre::Vector3 _position;

        OgreBulletCollisions::BoxCollisionShape* chassisShape;
        OgreBulletCollisions::CompoundCollisionShape* compound;
        OgreBulletDynamics::WheeledRigidBody  *mCarChassis;
        OgreBulletDynamics::VehicleTuning     *mTuning;
        OgreBulletDynamics::VehicleRayCaster  *mVehicleRayCaster;
        OgreBulletDynamics::RaycastVehicle    *mVehicle;

        float gWheelRadius = 0.6f;
        float gWheelWidth = 0.31f;
        float gWheelFriction = 1e30f;
        float gRollInfluence = 0.1f;
        float gSuspensionRestLength = 0.6;
        float gEngineForce = 5000.0f;
        float mSteering;
        bool up, down, left, right, shoot;

        Ogre::Entity    *mChassis;
        Ogre::Entity    *mWheels[4];
        Ogre::SceneNode *mWheelNodes[4];
        Ogre::SceneNode *chassisnode;

        Ogre::Camera *camera;
        Ogre::SceneNode *cameraNode;

        int num_disp=0;
        Ogre::ParticleSystem* particleSystem;
        Ogre::SceneNode* node2;
        Ogre::ParticleSystem* particleSystemDie;
        Ogre::SceneNode* node2Die;


        void AddDynamicObject();

        btTransform transform;
        int* frameCount;

        enum controlType{
          JOYSTICK_CONTROL = 1,
          KEYBOARD_CONTROL,
          WIIMOTE_CONTROL,
          IA_CONTROL
        };

        enum gameMode{
          SINGLE_PLAYER = 1,
          MULTIPLAYER,
          ONLINE
        };

        int targetPoint;

        void moveIA(std::vector<PointIA>*);


};

#endif //CAR_H
