#ifndef STATEI_H
#define STATEI_H

#include "State.h"
#include "OnlineSubscriberThread.h"

using namespace std;
using namespace Ice;
using namespace Online;

class StateI : public State{
public:
  StateI(std::vector<StateInfo>*, bool *);
  void sendState(const StateInfo& s, const Current& current);
  void setStart(const bool start, const Current& current);


private:
  std::vector<StateInfo>*  _playersInfo;
  bool *_start;

};

#endif //STATEI_H
