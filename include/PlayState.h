/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include "cegui-0/CEGUI/CEGUI.h"
#include <CEGUI/RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include <iostream>
#include <string>
#include <stdlib.h> 
#include <time.h>
#include "yaml-cpp/yaml.h"
#include <OGRE/OgreTimer.h>
#include <OGRE/Ogre.h>
#include <OgreBullet/Dynamics/OgreBulletDynamicsRigidBody.h>
#include <OgreBullet/Collisions/OgreBulletCollisions.h>
#include <cegui-0/CEGUI/CEGUI.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsBoxShape.h>
#include <OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include "OgreBullet/Collisions/Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "OgreBullet/Collisions/Shapes/OgreBulletCollisionsSphereShape.h"
#include "OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBullet/Dynamics/Constraints/OgreBulletDynamicsHingeConstraint.h"
#include "OgreBullet/Dynamics/Constraints/OgreBulletDynamicsPoint2pointConstraint.h"
#include "OgreBullet/Collisions/Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "OgreBullet/Dynamics/OgreBulletDynamicsConstraint.h"
#include "OgreBullet/Dynamics/Constraints/OgreBulletDynamicsHingeConstraint.h"
#include "bullet/BulletCollision/CollisionShapes/btUniformScalingShape.h"
#include "bullet/BulletDynamics/Dynamics/btRigidBody.h"
#include "OgreBullet/Collisions/Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "bullet/BulletDynamics/Dynamics/btRigidBody.h"
#include "OgreBullet/Collisions/Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "bullet/btBulletCollisionCommon.h"
#include "bullet/BulletCollision/btBulletCollisionCommon.h"
#include "bullet/BulletCollision/CollisionDispatch/btCollisionWorld.h"
#include "OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h"
#include "OgreBullet/Dynamics/Constraints/OgreBulletDynamicsRaycastVehicle.h"
#include "OgreBullet/Collisions/Debug/OgreBulletCollisionsDebugDrawer.h"

#include "cegui-0/CEGUI/RendererModules/Ogre/Renderer.h"
#include "cegui-0/CEGUI/RendererModules/Ogre/Texture.h"

#include <CEGUI/RendererModules/Ogre/Renderer.h>
#include "cegui-0/CEGUI/ImageManager.h"
#include <cmath>
#include <deque>
#include <vector>

#include "Car.h"
#include "Player.h"

#include "OnlinePublisher.h"
#include "OnlineSubscriberThread.h"

#define RAY_QUERY_NONE 1 << 1
#define RAY_QUERY_CAR 1 << 2


class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  void povMoved( const OIS::JoyStickEvent &e, int pov );
  void axisMoved( const OIS::JoyStickEvent &e, int axis, int joystick );
  void sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
  void buttonPressed( const OIS::JoyStickEvent &e, int button, int joystick );
  void buttonReleased( const OIS::JoyStickEvent &e, int button, int joystick );

  static PlayState& getSingleton();
  static PlayState* getSingletonPtr();

  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);

  void createGUI();  
  void createScene();
  void pauseGame();
  void updateTime();
  //void updateRecord(int score);
  bool returnMenu();
  void showEnd(int idWinner);
  void saveRecord(const CEGUI::EventArgs &e);
  bool onMouseEnters(const CEGUI::EventArgs& e);
  void checkCollisions();
  void moveCars();
  void setShoots();
  void hideLoadingPopUp();

  void createMiniMap();


  enum controlType{
      JOYSTICK_CONTROL = 1,
      KEYBOARD_CONTROL,
      WIIMOTE_CONTROL,
      IA_CONTROL
  };

  enum gameMode{
      SINGLE_PLAYER = 1,
      MULTIPLAYER,
      ONLINE
  };


 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;

  Ogre::Timer* _timeCounter;
  unsigned long _startTime;
  unsigned long _pauseTime;
  unsigned long _exitTime;
  int _timeCount;
  int _endTime;

  bool _exitGame;
  bool _inGame;
  bool _gameOver;

  CEGUI::DefaultWindow* _textBoxTime;
  CEGUI::Window* _editName;

  int _mode;
  int _control_1, _control_2;
  int _music;
  int _numCars;
  int _id;


  OgreBulletDynamics::DynamicsWorld * _world;
  OgreBulletCollisions::DebugDrawer * _debugDrawer;

  Car* vehicles[10];
  Player* players[2];

  OgreBulletDynamics::RigidBody * rigidTrack;
  OgreBulletCollisions::TriangleMeshCollisionShape *trackTrimesh;

  OgreBulletDynamics::RigidBody * rigidTrackP;
  OgreBulletCollisions::TriangleMeshCollisionShape *trackTrimeshP;

  OgreBulletDynamics::RigidBody * rigidFinish;
  OgreBulletCollisions::TriangleMeshCollisionShape *finishTrimesh;

  //CEGUI::AnimationInstance* travelRotateInInst;
  CEGUI::Window* _loadingWindow;
  CEGUI::Window* _trafficLight;
  CEGUI::Window* _speedometer;
  CEGUI::Window* _speedometerAguja;
  CEGUI::Window* _losePopup;

  CEGUI::Window* sheet;
  CEGUI::Window* configWin;

  Ogre::Rectangle2D* mMiniScreen;
  Ogre::SceneNode* miniScreenNode;
  Ogre::MaterialPtr renderMaterial;
  Ogre::Camera *_mmCamera;
  Ogre::RenderTexture* renderTextureMM;

  int collition;
  int timeCol;
  int frameCountShoot;
  int frameCountRaycast;

  int frameCountCollision;

  Ogre::RaySceneQuery *_rayFinishLine;
  Ogre::RaySceneQuery *_rayPass1;

  OnlinePublisher* op;

  OnlineSubscriberThreadPtr ost;
  std::vector<StateInfo> onlineVector;
  StateInfo localCar;
  btQuaternion rot;
  btVector3 lv;
  btVector3 av;

  void updateSpeedometer();

  void checkFinishLine();
  void checkPass1();

  float fTotalAppTime = 0.0f;     // total application time since start
  float interpTime = 10.0f;  // interpolation time in seconds

  Ogre::SimpleSpline *m_spline;

    int iaCounterStrikeGlobalOffensive;

};

#endif
