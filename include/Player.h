
#ifndef PLAYER_H
#define PLAYER_H

#include <Ogre.h>
#include <OGRE/Ogre.h>
#include <IceUtil/IceUtil.h>
#include "WiimoteThread.h"


struct PointIA{
    float x;
    float y;
    float z;
    bool up;
    bool down;
    double steer;
};

class Player{
    public:
        Player(int, int);
        ~Player();
        Player();
        static int count;

        int getId();
        int getVehicleId();
        int getControl();
        WiimoteThreadPtr getWiimote();
        std::vector<PointIA>* getPointsIA();

    protected:
        int id;
        int vehicleId;
        int controlType;

        enum controlType{
          JOYSTICK_CONTROL = 1,
          KEYBOARD_CONTROL,
          WIIMOTE_CONTROL,
          IA_CONTROL
        };

        enum gameMode{
          SINGLE_PLAYER = 1,
          MULTIPLAYER,
          ONLINE
        };




    std::vector<PointIA>* v_points;
    WiimoteThreadPtr wii;


};

#endif //PLAYER_H
