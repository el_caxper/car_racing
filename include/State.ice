// -*- mode:c++ -*-

module Online {

  struct StateInfo {
	int iden;        
	bool up;
	bool down;
	bool shoot;
	double steering;
	double posX;
	double posY;
	double posZ;
	double rotX;
	double rotY;
	double rotZ;
	double rotAngle;
	/*double avX;
	double avY;
	double avZ;
	double lvX;
	double lvY;
	double lvZ;  */   
};

  interface State {
    void sendState(StateInfo s);
    void setStart(bool start);
  };
};
