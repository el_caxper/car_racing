/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef MenuState_H
#define MenuState_H

#include <iostream>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <cegui-0/CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>
#include "GameState.h"

class MenuState : public Ogre::Singleton<MenuState>, public GameState
{
 public:
  MenuState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  void povMoved( const OIS::JoyStickEvent &e, int pov );
  void axisMoved( const OIS::JoyStickEvent &e, int axis, int joystick );
  void sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
  void buttonPressed( const OIS::JoyStickEvent &e, int button, int joystick );
  void buttonReleased( const OIS::JoyStickEvent &e, int button, int joystick );

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static MenuState& getSingleton ();
  static MenuState* getSingletonPtr ();

  void createGUI();
  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
  bool quit(const CEGUI::EventArgs &e);
  bool play(const CEGUI::EventArgs &e);
  bool show_records(const CEGUI::EventArgs &e);
  void mostrar_records();
  bool back_menu(const CEGUI::EventArgs &e);
  void show_credits();

  CEGUI::AnimationInstance* parpadeo_anim;
  CEGUI::AnimationInstance* parpadeo_rot_anim;
  CEGUI::AnimationInstance* viaje_meteor;
  CEGUI::AnimationInstance* rotacion_loop;

  bool onMouseEnters(const CEGUI::EventArgs& e);

  void show_config();
  void aplicar_conf();

  void game_mode_changed();




 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;

  CEGUI::Window* configWin;

  CEGUI::Combobox* combobox_tipo_juego;
  CEGUI::Combobox* combobox_control_1;
  CEGUI::Combobox* combobox_control_2;
  CEGUI::ToggleButton* check_music;


  bool _config_window_atcive;


  int _mode;
  int _control_1, _control_2;
  int _music;
  int _numCars;
  int _id;

  bool _exitGame;
};

#endif
