#ifndef ONLINESUBSCRIBERTHREAD_H
#define ONLINESUBSCRIBERTHREAD_H

#include <Ice/Ice.h>
#include <IceStorm/IceStorm.h>
#include <IceUtil/IceUtil.h>
#include "State.h"
#include "StateI.h"

using namespace std;
using namespace Ice;
using namespace IceStorm;
using namespace Online;

class OnlineSubscriberThread : public IceUtil::Thread {
public:
    OnlineSubscriberThread(int);
    ~OnlineSubscriberThread();

    virtual void run();
    std::vector<StateInfo> getOnlineInfo();
    bool getStartInfo();
    void showOnlineInfo();

    Ice::CommunicatorPtr ic;

private:
    IceUtil::Time _delay;  // Delay during updates
    IceUtil::Mutex _mutex; // Mutex lock
    std::vector<StateInfo> onlinePlayers;
    bool _start;
    int _numPlayers;
    TopicPrx topic;
    ObjectPrx base;
};


typedef IceUtil::Handle<OnlineSubscriberThread> OnlineSubscriberThreadPtr; // Smart pointer

#endif //ONLINESUBSCRIBERTHREAD_H
