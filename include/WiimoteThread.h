#ifndef __WiimoteThread__
#define __WiimoteThread__

#include <IceUtil/IceUtil.h>
#include <cmath>

extern "C" {
#include "wiimote_api.h"
}

class WiimoteThread : public IceUtil::Thread {

 public:
  WiimoteThread();
  WiimoteThread(int);
  void enableRumble(bool);
  int getKeysBits();
  int getKeysOne();
  int getKeysTwo();
  int getKeysPlus();
  int getKeysMinus();
  double getAngleZ();
  void update();
  wiimote_t getWiimoteData();
  void setConnected(bool);

  virtual void run ();

 private:
  IceUtil::Time _delay;  // Delay during updates
  IceUtil::Mutex _mutex; // Mutex lock
  wiimote_t _wiimote; // Wiimote struct
  void connect();
  bool connected;
};

typedef IceUtil::Handle<WiimoteThread> WiimoteThreadPtr; // Smart pointer

#endif
