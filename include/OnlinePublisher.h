#ifndef ONLINEPUBLISHER_H
#define ONLINEPUBLISHER_H

#include <Ice/Ice.h>
#include <IceStorm/IceStorm.h>
#include "State.h"

using namespace std;
using namespace Ice;
using namespace IceStorm;
using namespace Online;

class OnlinePublisher {
public:
    OnlinePublisher();
    ~OnlinePublisher();

    StatePrx statePrx;

    StatePrx getStatePrx();

    Ice::CommunicatorPtr ic;
};

#endif //ONLINEPUBLISHER_H
